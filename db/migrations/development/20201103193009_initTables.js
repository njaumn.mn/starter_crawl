
exports.up = function(knex, Promise) {
    return knex.schema.hasTable('job').then(function (exists) {
        if (!exists) {
            return knex.schema.createTable('job', function (table) {
                table.increments('id').primary();
                table.string('jobId', 60).notNullable();
                table.string('status', 20).notNullable();
                table.string('type', 20).notNullable();
                table.longtext('input').notNullable();
                table.longtext('output');
                table.mediumtext('errors');
            }).then(console.log('created jobs table'));
        }
    });
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists('job');
};
