// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: __dirname + '/db/yangu'
    },
    seeds: {
      directory: __dirname + '/db/seeds/development'
    },
    migrations: {
      directory: __dirname + '/db/migrations/development'
    },
    useNullAsDefault: true
  }

};

