# Arachno Scripts QC Process
Once you're done with writing a script, submit it for quality control by creating a sub-folder here with the same name as the script.
Please make sure that whoever is reviewing your script has read access to your fork.

## Scraping Scripts
For scraping scripts we want 15 sample documents that were scraped to be uploaded to the folder. A file for each resulting HTML file, just named 01.html, 02.html, 03.html .... 15.html. And a file for each corresponding meta data in json, named 01.json, 02.json .... 15.json, where the numbers correspond to the respective HTML document.

## Conversion Scripts
Add the output of the converted scripts to the above mentioned folder, and use the same naming convention, but with a _converted suffix: 01_converted.html, 02_converted.html, ... 15_converted.html

