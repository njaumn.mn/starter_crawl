# Arachno Scripts

Simple server to test out arachno scripts. Arachno scripts should fetch, track and convert documents to LegalDocHTML.


## Getting started

### Requirements

- Knowledge of JavaScript, NodeJS and TypeScript.
- NodeJS v12+.
- Yarn package manager.

### Development

- Clone the repository.
- Run `yarn install` to get the dependencies.
- Run `cp .env.example .env` to setup local development variables.
- Initialize the database - `yarn knex migrate:latest`
- Run `yarn start` on one terminal to keep the typescript compile running and start the local development server.
- Open http://localhost:4200/ to view log output.


## Notes
- All scripts go in the `scripts` directory - grouped by country using [ISO 3166-1 alpha-2 codes](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements)
- To test a script you're working on, simply hit the correct endpoint. Sample requests below. [Can also check this screenshare for extra info on the endpoints](https://youtu.be/Sfju7Po6Hh4)

```
### sourceId matches the 'sourceSlug' property of the script you need to test -- format: [ISO 3166-1 alpha-2 codes]-[source name]

### Initial crawl sourceID - OK
POST http://localhost:4200/initial-crawl
content-type: application/json

{
    "sourceId": "us-qcode"
}


### Updates check SourceID - OK
POST http://localhost:4200/get-updates
content-type: application/json

{
    "sourceId": "EurLex"
}


### Capture convert single doc   - OK
POST http://localhost:4200/get-doc
content-type: application/json
 
{
    "sourceId": "EurLex",
    "startUrl": "http://www.legislation.gov.uk/id/ssi/2020/291"

}


### Fetch metadata  https://discover.sabinet.co.za/document/NTL15079  - OK
POST http://localhost:4200/get-metadata
content-type: application/json
 
{
    "sourceId": "EurLex",
    "startUrl": "http://www.legislation.gov.uk/id/ssi/2020/291"

}


### Convert to legaldochtml   - OK
POST http://localhost:4200/convert-to-legaldoc
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
sourceId: us-qcode

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="doc"; filename="[YOUR_FILE_NAME].html"
Content-Type: text/html

< ./[PATH_TO_YOUR_FILE].html
------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="ProspectId"

1
------WebKitFormBoundary7MA4YWxkTrZu0gW--


```

- You can test the above requests as-is in VSCode REST client extension OR replicate the same in Postman