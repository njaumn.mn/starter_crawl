module.exports = {
  apps: [
    {
      name: 'arachno',
      script: './dist/server.js',
      instances: 'max',
      exec_mode: 'cluster',
    },
  ],
};
