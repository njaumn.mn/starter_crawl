import * as dotenv from 'dotenv';
import convict from 'convict';
dotenv.config();

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'staging', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  port: {
    doc: 'The port express is served on.',
    format: Number,
    default: 4200,
    env: 'APP_PORT',
  },
  emogrifier: {
    host: {
      doc: 'The Emogrifier service host',
      default: 'http://emogrifier.test',
      env: 'EMOGRIFIER_HOST',
    },
  }
});

export default config;
