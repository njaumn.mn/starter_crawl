interface ArachnoGlobal extends NodeJS.Global {
  ARACHNO: any;
}

declare const global: ArachnoGlobal;