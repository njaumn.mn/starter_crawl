import { header, body, validationResult } from "express-validator";
import { messages } from "./messages";

export const initialNewPayloadValidator = () => {
    return [
        body('sourceId', messages.SOURCE_ID_MISSING_DEFAULT).trim()
            .isLength({min: 2}).withMessage(messages.SOURCE_ID_TOO_SHORT)
            .notEmpty().withMessage(messages.SOURCE_ID_REQUIRED)
            .optional({ nullable: true, checkFalsy: false }).withMessage(messages.SOURCE_ID_REQUIRED),
        body('domain', messages.DOMAIN_MISSING_DEFAULT)
            .isLength({min: 2}).withMessage(messages.DOMAIN_TOO_SHORT)
            .notEmpty().withMessage(messages.DOMAIN_REQUIRED)
            .optional({ nullable: true, checkFalsy: false }).withMessage(messages.DOMAIN_REQUIRED),
        body('jurisdictions', messages.JURISDICTIONS_MISSING_DEFAULT)
            .notEmpty().withMessage(messages.JURISDICTIONS_MUST_BE_ARRAY)
            .isArray().withMessage(messages.JURISDICTIONS_MUST_BE_ARRAY)
            .optional({ nullable: true, checkFalsy: false }).withMessage(messages.JURISDICTIONS_ARR_REQUIRED),
    ]
}

export const initialNewPayloadValidatorCustom = (req, res, next) => {
    const extractedErrors = [];
    const errors = validationResult(req);
    
    if (
        (!req.body.domain && !req.body.sourceId ) || 
        (req.body.jurisdictions && !req.body.domain) ||
        (req.body.domain && req.body.sourceId)
    ) {
        return res.status(422).json({
            errors: [{
                "badRequest": "'domain' & 'sourceId' are mutually exclusive - at least one required. 'jurisdictions' array cannot be provided without specifying 'domain'"
            }],
          });
    } else if (errors.isEmpty()) {
        return next();
    }

    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));
    
    return res.status(422).json({
      errors: extractedErrors,
    });
}

export const capturePayloadValidator = () => {
    return [
        body('sourceId', messages.SOURCE_ID_MISSING_DEFAULT).trim()
            .isLength({min: 2}).withMessage(messages.SOURCE_ID_TOO_SHORT)
            .notEmpty().withMessage(messages.SOURCE_ID_REQUIRED)
            .exists({ checkFalsy: true, checkNull: true}).withMessage(messages.SOURCE_ID_REQUIRED),
        body('startUrl', messages.START_URL_MISSING_DEFAULT)
            .isLength({min: 2}).withMessage(messages.START_URL_TOO_SHORT)
            .notEmpty().withMessage(messages.START_URL_REQUIRED)
            .exists({ checkFalsy: true, checkNull: true}).withMessage(messages.START_URL_REQUIRED),
    ]
}

export const defaultPayloadValidatorCustom = (req, res, next) => {
    const extractedErrors = [];
    const errors = validationResult(req);
    
    if (errors.isEmpty()) {
        return next();
    }

    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }));
    
    return res.status(422).json({
      errors: extractedErrors,
    });
}

export const convertPayloadValidator = () => {
    return [
        header('sourceId', messages.SOURCE_ID_MISSING_DEFAULT).trim()
            .isLength({min: 2}).withMessage(messages.SOURCE_ID_TOO_SHORT)
            .notEmpty().withMessage(messages.SOURCE_ID_REQUIRED)
            .exists({ checkFalsy: true, checkNull: true}).withMessage(messages.SOURCE_ID_REQUIRED),
    ]
}