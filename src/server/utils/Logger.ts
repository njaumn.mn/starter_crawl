import * as winston from "winston";
import 'winston-daily-rotate-file';

declare const global: ArachnoGlobal;

export default class Logger {
  
  /**
   * Initializes the logger and adds it to the ARACHNO namespace
   */
  static init() {
    if (!global.ARACHNO) {
        global.ARACHNO = {};
    }

    global.ARACHNO.logger = Logger.configLogger();
  }

   static configLogger(){
    //define custom log format
    const logFormat = winston.format.combine(
      winston.format.splat(),
      winston.format.json(),
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.align(),
      winston.format.printf(
        info => `${info.timestamp} ${info.level}: ${info.message}`,
      ),
    );

    //create Logger
    return winston.createLogger({
      format: logFormat,
      transports: [
        new winston.transports.Console({
          level: 'debug',
          handleExceptions: true,
        }),
        new winston.transports.DailyRotateFile({
          filename: './logs/arachnoDailyLogs-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          level: 'debug',
        }),
      ],
    });
  }
}

