import jsdom from 'jsdom';


export default class DOMLoader {
  static html = '<html><head></head><body></body></html>';

  /**
   * Properties we need loaded from the browser
   * Only load those needed
   */
  static keys = [
    'DOMParser',
    'DOMException',
    'NamedNodeMap',
    'Attr',
    'Node',
    'Element',
    'HTMLDocument',
    'Document',
    'Location',
    'History',
    'XPathException',
    'XPathExpression',
    'XPathResult',
    'XPathEvaluator',
    'HTMLCollection',
    'NodeFilter',
    'NodeIterator',
    'NodeList',
    'XMLHttpRequest',
    'XMLHttpRequestEventTarget',
    'XMLHttpRequestUpload',
    'DOMTokenList',
    'MutationObserver',
    'window',
    'document',
  ];

  /**
   * Initializes the Node environment with browser elements
   */
  static init(): void {
    const document = new jsdom.JSDOM(DOMLoader.html);
    const window = document.window;

    if (!global.ARACHNO) {
      global.ARACHNO = {};

      DOMLoader.keys.forEach((key) => {
        global['ARACHNO'][key] = window[key];
      });

      global.ARACHNO.document = window.document;
      global.ARACHNO.window = window;
    }
  }
}
