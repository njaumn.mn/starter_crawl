import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import IDocument from '@app/interfaces/IDocument';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import CaptureBase from '@app/modules/CaptureBase';
import Document from '@app/modules/Document';
import * as _url from 'url';
import { Page } from 'playwright';
import _ from 'lodash';
import { dataTypes } from '@app/interfaces/ICapture';


/**
 * Has the states initials we expect from the Request and the various
 * mappings that we make to various endpoints.
 */
interface ISouthAfricaStates {
  ec: string;
  fs: string;
  gt: string;
  nl: string;
  lp: string;
  mp: string;
  nw: string;
  nc: string;
  wc: string;
}

/**
 * Main Capture class
 */
class SabinetCapture extends CaptureBase {

  baseUrl: string;
  stateNames: ISouthAfricaStates;
  stateCodes: ISouthAfricaStates;
  sleepInterval: number;
  constructor(params: any) {
    super(params);
    this.baseUrl = 'https://discover.sabinet.co.za';
    this.stateNames = {
      ec: 'Eastern Cape',
      fs: 'Free State',
      gt: 'Gauteng',
      nl: 'KwaZulu-Natal',
      lp: 'Limpopo',
      mp: 'Mpumalanga',
      nw: 'North West',
      nc: 'Northern Cape',
      wc: 'Western Cape',
    };
    this.stateCodes = {
      ec: 'ECP',
      fs: 'FSP',
      gt: 'GAU',
      nl: 'KZN',
      lp: 'LIM',
      mp: 'MP',
      nw: 'NWP',
      nc: 'NCP',
      wc: 'WCP',
    };
    this.sleepInterval = 1500;
  }
  public sourceSlug = 'sa-sabinet';

  public domain = 'za'; // South Africa

  public jurisdictions = ['ec', 'fs', 'gt', 'nl', 'lp', 'mp', 'nw', 'nc', 'wc'];

  public params: ICaptureParameters;

  protected dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = null;

  public async beforeEmogrifyHook(doc: String): Promise<String>{
    return doc;
  }

  /**
   * Implements logic to login before making requests
   */
  protected async login() {
    console.log('Logging into Sabinet first');
    try {
      const page = await this.getNewPage();
      await page.goto(`${this.baseUrl}/login`);

      const welcome = await page.$('text=Welcome...');
      if (welcome) return page;

      await page.fill('#signin_username', '038a');
      await page.fill('#signin_password', 'envl');
      await page.click('css=input[value="Sign In"');

      await page.waitForSelector('text=Welcome...');

      return page;
    } catch (error) {
      return Promise.reject(`Error logging into Sabinet: ${error}`);
    }
  }

  public async beforeConvertHook(doc: IDocument): Promise<IDocument> {
    doc.optimusScript = 'EurLex';
    return doc;
  }

  /**
   * Fetches all the National Law links. Includes PDF documents links
   * @param url parameter pointing to the National Law links
   */
  async getNationalLaw(url: string): Promise<string[]> {
    console.log('Getting Sabinet National Law');
    const links = [];
    try {
      const page = await this.login();
      await page.goto(url);

      const actsList = await page.$$('css=td>a');
      const alphabeticalActsList = [];
      for (const actAnchor of actsList) {
        const actHref = await actAnchor.getAttribute('href');
        if (actHref) {
          alphabeticalActsList.push(`${this.baseUrl}${actHref}`);
        }
      }

      // NOTE: asynchronous code using Promise.all() would be ideal
      // but Sabinet will blacklist you and browsers will probably
      // crash too.

      // for (const actLink of alphabeticalActsList.slice(0, 10)) {
      for (const actLink of alphabeticalActsList) {
        try {
          const newPage = await page.context().newPage();

          console.log(`Sleeping for ${this.sleepInterval} secs.`);
          await new Promise((r) => setTimeout(r, this.sleepInterval));
          await newPage.goto(actLink);

          const theActUrlObj = await newPage.$('css=p>span>a,p>a');
          const theActTxt = await theActUrlObj.textContent();
          if (theActTxt.trim() === 'THE ACT') {
            const theActHref = await theActUrlObj.getAttribute('href');
            const theActUrl = `${this.baseUrl}/webx/access/netlaw/${theActHref}`;
            links.push(theActUrl);
            await newPage.close();
          }
        } catch (error) {
          return Promise.reject(`Error getting act: ${error}`);
        }
      }
      await page.close();
    } catch (error) {
      return Promise.reject(`Error getting Sabinet national law: ${error}`);
    }
    // return links.slice(0, 5);
    return links;
  }

  /**
   * Gets all the law in the Provincial level. If jurisdictions are provided, then all the laws
   * in the specified jurisdictions are fetched.
   * @param url
   * @param jurisdictions
   */
  async getProvincialLaw(url: string, jurisdictions: string[]): Promise<string[]> {
    console.log('Getting Sabinet Provincial Laws');
    const links = [];

    try {
      const page = await this.login();
      await page.goto(url);

      await this.checkJurisdictionBoxesInPage(
        jurisdictions,
        page,
        '#lpaSearch_province_',
        this.stateNames
      );

      const searchCount = await this.getPageSearchResultsCount(page);
      const pages = this.getPagesPerPage(searchCount, 20);
      const page1 = await this.getThePageUrls(page);
      links.push(...page1);

      await this.getTheRestOfTheLinks(pages, page, links);
      await page.close();
    } catch (error) {
      return Promise.reject(`Error getting the Sabinet provincial laws: ${error}`);
    }

    // return links.slice(0, 5);
    return links;
  }

  /**
   * Gets all the law in the Municipal By Laws level. If jurisdictions are provided, then all the laws
   * in the specified jurisdictions are fetched.
   * @param url
   * @param jurisdictions
   */
  async getMunicipalLaw(url: string, jurisdictions: string[]): Promise<string[]> {
    console.log('Getting Sabinet Municipal By-Laws');
    const links = [];
    try {
      const page = await this.login();
      await page.goto(url);

      await this.checkJurisdictionBoxesInPage(
        jurisdictions,
        page,
        '#blwSearch_province_',
        this.stateCodes
      );
      const searchCount = await this.getPageSearchResultsCount(page);
      const pages = this.getPagesPerPage(searchCount, 10);
      const page1 = await this.getThePageUrls(page);
      links.push(...page1);

      await this.getTheRestOfTheLinks(pages, page, links);

      await page.close();
    } catch (error) {
      return Promise.reject(`Error getting Municipal Law of Sabinet: ${error}`);
    }
    // return links.slice(0, 5);
    return links;
  }

  /**
   * Get the rest of the links from the pages starting from Page 2 after a search.
   * @param pages Total number of pages in the search.
   * @param page instance of the page Browser
   * @param links got from the various pages
   */
  private async getTheRestOfTheLinks(pages: number, page: Page, links: any[]) {
    // for (const pg of _.range(2, 2 + 1)) {
    for (const pg of _.range(2, pages + 1)) {
      const pageUrl = `${this.baseUrl}/provincial_netlaw?pg=${pg}`;
      console.log(`Sleeping for ${this.sleepInterval} secs.`);
      await new Promise((r) => setTimeout(r, this.sleepInterval));
      await page.goto(pageUrl);
      const pageUrls = await this.getThePageUrls(page);
      links.push(...pageUrls);
    }
  }

  /**
   * Gets the number of documents in the search results.
   * @param page Instance of a page browser.
   */
  private async getPageSearchResultsCount(page: Page) {
    const searchStats = await page.waitForSelector('css=div.search_stats>strong');
    const searchResults = await searchStats.textContent();
    const searchCount = parseInt(searchResults, 10);
    return searchCount;
  }

  /**
   * Helper to check all the juristdictions in the search form.
   * @param jurisdictions that are being searched.
   * @param page instance of the browser.
   * @param selector the identifier of the checkbox.
   * @param states the mappings of the states that are being used.
   */
  private async checkJurisdictionBoxesInPage(
    jurisdictions: string[],
    page: Page,
    selector: string,
    states: ISouthAfricaStates
  ) {
    const checkBoxes = jurisdictions.map((j) => `${selector}${states[j].replace(' ', '_')}`);

    await Promise.all(checkBoxes.map(async (cbx) => await page.check(cbx)));
    await page.click('css=input[value="Search"');
  }
  /**
   * Fetches the URL links in a given page instance.
   * @param page instance of the page browser.
   */
  private async getThePageUrls(page: Page): Promise<string[]> {
    const links = [];
    try {
      const results = await page.$$('css=div.result>h3>a');
      const resultsUrls = await Promise.all(
        results.map(async (res) => await res.getAttribute('href'))
      );
      resultsUrls.forEach((res) => links.push(res));
    } catch (error) {
      return Promise.reject(`Error getting the Provincial Page link in Sabinet: ${error}`);
    }
    return links;
  }

  /**
   * Calculates the number of pages a result will span. If you have 50 results,
   * with an offset of 5 you get 10 pages.
   * @param searchCount the total number of search results.
   * @param offset the number of documents/urls in a page.
   */
  private getPagesPerPage(searchCount: number, offset: number) {
    let pages: number;
    if (searchCount % offset !== 0) {
      pages = Math.floor(searchCount / offset) + 1;
    } else {
      pages = Math.floor(searchCount / offset);
    }
    return pages;
  }

  /**
   * Performs the initial crawl for links for a site.
   * @param jurisdictions
   */
  public async initialCrawl(jurisdictions?: string[]): Promise<string[]> {
    console.log('Perfoming Sabinet Initial Crawl');
    const linksArr = [];

    try {
      const { sourceId, domain } = this.params;
      if ((domain && jurisdictions.length === 0) || sourceId) {
        console.log('Just the domain or sourceId, no jurisdictions');
        const nationalUrl = `${this.baseUrl}/inline_html?file=NETLAW_ALPHABETICAL_INDEX.html`;
        const nationalLinks = await this.getNationalLaw(nationalUrl);
        linksArr.push(...nationalLinks);
      } else {
        const provincialUrl = `${this.baseUrl}/provincial_netlaw`;
        const municipalUrl = `${this.baseUrl}/municipal_by_laws`;

        const jurisdictionLinks = await Promise.all([
          this.getMunicipalLaw(municipalUrl, jurisdictions),
          this.getProvincialLaw(provincialUrl, jurisdictions),
        ]);

        jurisdictionLinks.forEach((l) => linksArr.push(...l));
      }
      return linksArr;
    } catch (e) {
      return Promise.reject(`Error in initialCrawl: ${e}`);
    }
  }

  /**
   * Checks for new works on the update feed and returns them
   */
  public async getUpdates(): Promise<string[]> {
    const links = [];
    return links;
  }

  public setParameters(params: ICaptureParameters) {
    this.params = params;
  }

  public async getMetaData(url: string): Promise<IWorkMetaData> {
    const { docNumber, isMunicipal, isProvincial, isPDF } = this.checkURLType(url);
    const meta = {
      language_code: 'en',
      primary_location_id: 21,
      source_unique_id: this.params.sourceUniqueId,
      source_id: 1,
      issuing_authority_id: 44,
      start_url: url,
      title: null,
      effective_date: new Date(),
      content_hash: this.params.content_hash, // Will be set in Capture class
      title_translation: null,
      work_number: docNumber,
      publication_number: null,
      publication_document_number: null,
      work_type: null, // Will be set in Capture class
      work_date: new Date(),
      sourceSlug: null,
    };
    if (isPDF) {
      return meta;
    }
    try {
      if (isProvincial || isMunicipal) {
        const page = await this.login();
        await page.goto(url);
        const titleChildNum = isProvincial ? 2 : 1;
        const titleObj = await page.$(
          `css=div.metadata>table.document_meta>tbody>tr:nth-child(${titleChildNum})>td.value_td`
        );
        const title = await titleObj.textContent();
        meta.title = title.trim();

        const dateChildNum = isProvincial ? 8 : 4;
        const dateObj = await page.$(
          `css=div.metadata>table.document_meta>tbody>tr:nth-child(${dateChildNum})>td.value_td`
        );
        const date = await dateObj.textContent();
        meta.work_date = new Date(date.trim());
      } else {
        // TODO: capture the exact dates from the document
        // It's a national or a PDF
        const urlObj = new URL(url);
        const urlPathName = urlObj.pathname;
        const lastPathName = urlPathName.split('/').pop();
        const docRx = /(\d+)_(\d+)_(\w+)\./gm;
        const match = docRx.exec(lastPathName);
        if (match) {
          const docNum = match[1];
          const docYear = match[2];
          const docTitle = match[3].replace(/_/gm, ' ').toUpperCase();
          meta.work_number = docNum;
          meta.work_date = new Date(docYear);
          meta.title = `${docTitle} ${docNum} of ${docYear}`;
        }
      }
    } catch (error) {
      return Promise.reject(`Error getting document metadata in Sabinet: ${error}`);
    }
    return meta;
  }

  /**
   * Retrieves the document stored at the url passed and returns it as an IDocument
   * denoting the format that we retrieved
   * @param url the direct url to a single document
   */
  public async getDoc(url: string): Promise<IDocument | null> {
    const doc = new Document(this.sourceSlug);
    const { isPDF, isMunicipal, isProvincial } = this.checkURLType(url);
    if (isPDF) {
      // TODO: handle fetching and converting a PDF to base64
      // const response = await fetch(url);
      // const base64Pdf = response.body.toString('base64');

      // doc.doc = base64Pdf;
      doc.mimeType = 'application/pdf';
      doc.extension = 'pdf';
      // doc.generateHash()
      return doc;
    }

    try {
      const page = await this.login();
      await page.goto(url);

      let htmlDoc: string = '';
      if (isMunicipal || isProvincial) {
        const optimusScript = isMunicipal ? 'sabinet-bylaw' : 'sabinet-provincial';
        doc.optimusScript = optimusScript;

        const htmlDocObj = await page.$('css=div.revision_content');
        const htmlIframe = await page.$('css=iframe');
        if (htmlDocObj) {
          htmlDoc = await htmlDocObj.innerHTML();
        }

        if (htmlIframe) {
          htmlDoc = await htmlIframe.innerHTML();
        }
      } else {
        doc.optimusScript = 'sabinet-netlaw';
        const htmlDocObj = await page.$('css=div:nth-of-type(2),div.WordSection1');
        htmlDoc = await htmlDocObj.innerHTML();
      }

      doc.doc = htmlDoc;
      doc.mimeType = 'text/html';
      doc.extension = 'html';
      doc.generateHash();
      return doc;
    } catch (error) {
      return Promise.reject(`Error getting Sabinet Documet ${url}: ${error}`);
    }
  }

  /**
   * Checks if the URL is PDF (mostly national), Municipal or Provincial.
   * @param url the URL being checked
   */
  private checkURLType(url: string) {
    const urlObj = new URL(url);
    const urlPathName = urlObj.pathname;
    const municipalRx = /BLW\d+/gm;
    const provincialRx = /PN(?:A|R)\d+/gm;
    const isMunicipal = urlPathName.match(municipalRx) ? true : false;
    const isProvincial = urlPathName.match(provincialRx) ? true : false;
    const isPDF = urlPathName.toLowerCase().endsWith('.pdf') ? true : false;
    let docNumber: string;
    if (isProvincial || isMunicipal) {
      docNumber = urlPathName.match(/(?:BLW|PN(?:A|R))\d+/gm).pop();
    }
    return { isPDF, isMunicipal, isProvincial, docNumber };
  }


  public toLegalDoc(doc: IDocument): Promise<IDocument> {
    throw new Error('Method not implemented.');
  }
}

export default SabinetCapture;
