import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import IDocument from '@app/interfaces/IDocument';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import CaptureBase from '@app/modules/CaptureBase';
import { ElementHandle } from 'playwright';
import Document from '@app/modules/Document';
import fetch from 'node-fetch';
import * as _url from 'url';
import _ from 'lodash';
import { dataTypes } from '@app/interfaces/ICapture';

declare const global: ArachnoGlobal;

class LegislationGovCapture extends CaptureBase {
  public toLegalDoc(doc: IDocument): Promise<IDocument> {
    throw new Error('Method not implemented.');
  }
  public sourceSlug = `eu-eurLex`;

  public params: ICaptureParameters;

  public domain = 'gb-ukm';

  public jurisdictions = ['gb-sct', 'gb-wls', 'gb-nir']; // Doesn't have laws for england 'gb-eng'

  protected dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = null;

  public async beforeEmogrifyHook(doc: String): Promise<String>{
    return doc;
  }
  
  /**
   * Receives the document before it is converted to legaldoc. Can be used to preprocess
   * the html or select the appropriate Optimus script to use in conversion
   *
   * @param doc The html document to be converted to legalDoc
   */
  public async beforeConvertHook(doc: IDocument): Promise<IDocument> {
    return doc;
  }

  public initialCrawl(jurisdictions: string[]): Promise<string[]> {
    console.log('Perfoming EurLex Initial Crawl', jurisdictions);
    return new Promise(async (resolve, reject) => {
      try {
        const page = await this.getNewPage();

        let currentPage = 1;
        let nextPagesLink: ElementHandle | null = null;
        const linksArr = [];

        do {
          // Keep looping

          // 1 get all links from this page
          // 2 save to file

          await page.goto(`https://www.legislation.gov.uk/all?page=${currentPage++}`);

          nextPagesLink = await page.$('css=div.prevPagesNextNav>ul>li.pageLink.next');
          console.log({ Nextpage: nextPagesLink });

          let links = await page.$$('css=div#content.results>table tbody tr td:first-child a');

          for (let i = 0; i < links.slice(0,2).length; i++) { // TODO: Remove the slice in Production
            const item = links[i];
            let href = await item.getAttribute('href');
            href = href.replace(/\/contents$/i, '');
            const link = `https://www.legislation.gov.uk${href}`;
            linksArr.push(link);
          }
        } while (false); //nextPagesLink

        resolve(linksArr);
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Checks for new works on the update feed and returns them
   */
  public async getUpdates(jurisdictions: string[]): Promise<string[]> {

    const updateFeeds = {
      'gb-nir': 'https://www.legislation.gov.uk/new/nisr/data.feed',
      'gb-sct': 'https://www.legislation.gov.uk/new/ssi/data.feed', 
      'gb-wls': 'https://www.legislation.gov.uk/new/wsi/data.feed',
      'all': 'https://www.legislation.gov.uk/new/data.feed' 
    };
    let toSearch = ['all']; // Default behaviour - get all updates
    if (jurisdictions.length > 0) {
      toSearch = _.intersection(jurisdictions, Object.keys(updateFeeds));
      toSearch = toSearch.includes('all') ? ['all']: toSearch;
    }

    const pages = [];
    let newWorks = []; // Holds the update links discovered
    
    return new Promise(async (resolve, reject) => {
      try {
        for (const item in toSearch) {
          const jurisdiction = toSearch[item];

          const updateFeedLink = updateFeeds[jurisdiction];
          const page = await this.getNewPage();

          await page.goto(updateFeedLink);
          const newLawsFeed = await page.innerHTML('css=body>pre');

          const cleaner = global.ARACHNO.document.createElement('textarea');
          cleaner.innerHTML = newLawsFeed;
          const parser = new global.ARACHNO.DOMParser();
          const xmlDoc = parser.parseFromString(cleaner.value, 'text/xml');

          const updates = this.xpathFindAllNodes('//entry', xmlDoc);
          const utcDate = new Date().toJSON().slice(0, 10); //'2020-09-25';

          updates.forEach((elem) => {
            const publishedTodayRx = RegExp(`<published[^>]*?>${utcDate}`, 'gim');
            if (publishedTodayRx.test(elem.innerHTML)) {
              let docLink = /<link[^>]*?rel="self"[^>]*?href="(.*)"\/>/im.exec(elem.outerHTML);
              let link = docLink[1];
              if (/(.*?\/\d+\/\d+)\/\D+/gim.test(link)) {
                link = /(.*?\/\d+\/\d+)\/\D+/im.exec(link)[1];
              }
              newWorks.push(link);
              pages.push(page);
            }
          });

        }

        resolve(newWorks);        
      } catch (e) {
        console.error(`ERROR: Fetching Updates for ${this.sourceSlug} failed! Jurisdictions: ${ jurisdictions || 'ALL!' }`, e);
        reject(e);
      } finally {
        pages.forEach( page => page.close());
      }
    });
  }

  public setParameters(params: ICaptureParameters) {
    this.params = params;
  }

  public getMetaData(url: string): Promise<IWorkMetaData> {
    return new Promise(async (resolve, reject) => {
      try {
        let meta: IWorkMetaData;
        const page = await this.getNewPage();
        await page.goto(`${url}/data.xml`);

        // const docElem = await page.$('css=body');

        if (true) {
          //modify to select correct element
          // let docHTML = await docElem.innerHTML();

          meta = {
            title: 'Test title',
            language_code: 'en',
            primary_location_id: 21,
            source_unique_id: '11',
            source_id: 1,
            start_url: url,
            effective_date: new Date(),
            content_hash: null, // Will be set in Capture class
            title_translation: '',
            work_number: '',
            publication_number: '',
            publication_document_number: '',
            issuing_authority_id: 44,
            work_type: null, // Will be set in Capture class
            work_date: new Date(),

            sourceSlug: 'EurLex',
          };
        }

        resolve(meta);
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Retrieves the document stored at the url passed and returns it as an IDocument
   * denoting the format that we retrieved
   * @param url the direct url to a single document
   */
  public async getDoc(url: string): Promise<IDocument | null> {
    const doc = new Document(this.sourceSlug);
    return new Promise(async (resolve, reject) => {
      try {
        const page = await this.getNewPage();
        await page.goto(`${url}/data.html`);

        const docElem = await page.$(
          '*css=body >> css=article:first-child >> xpath=//div[@class="coverPage" or @class="preface" or @class="preamble" or @class="body"]'
        );

        //if have children coverPage || preface || preamble || body then html version available

        if (docElem) {
          const pageContent = await this.getPageData(page, {}, true);
          console.log(pageContent);
          
          let docHTML = await docElem.innerHTML();
          doc.doc = `<article>${docHTML}</article>`;
          doc.mimeType = 'text/html';
          doc.extension = 'html';
        } else {
          // Try getting PDF version maybe html version missing
          // Note: The PDF doc might sometimes be missing on this endpoint
          // especially for new document. In that case, fetch XML

          // Open the root url and get the anchor with class pdfLink

          await page.goto(url);
          const pdfLinkElem = await page.$('css=a.pdfLink');
          let pdfLink = await pdfLinkElem.getAttribute('href');

          if (!/^https?:\/\//gi.test(pdfLink)) {
            pdfLink = _url.resolve(page.url(), pdfLink);
          }

          const response = await fetch(pdfLink);
          const buffer = await response.buffer();
          const base64Pdf = buffer.toString('base64');

          // var fs = require('fs');
          // fs.writeFileSync('meeeeme.pdf', buffer);

          doc.doc = base64Pdf;
          doc.mimeType = 'application/pdf';
          doc.extension = 'pdf';
        }

        doc.generateHash();
        resolve(doc);
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Given an xpath returns all matching nodes in domObject
   * @param xpathToExecute xpath to execute on domObject
   * @param domobject domObject to perform search on
   */
  private xpathFindAllNodes(xpathToExecute: string, domobject) {
    const result: any = [];
    const nodesSnapshot = domobject.evaluate(
      xpathToExecute,
      domobject,
      null,
      global.ARACHNO.XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
      null
    );
    for (let i = 0; i < nodesSnapshot.snapshotLength; i += 1) {
      result.push(nodesSnapshot.snapshotItem(i));
    }
    return result;
  }
}

export default LegislationGovCapture;
