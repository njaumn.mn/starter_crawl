import CaptureBase from '@app/modules/CaptureBase';
import { dataTypes } from "@app/interfaces/ICapture";
import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import IDocument from '@app/interfaces/IDocument';
import Document from '@app/modules/Document';

declare const global: ArachnoGlobal;

export default class EurlexCapture extends CaptureBase {
  public sourceSlug: string = 'eur-lex-europa';

  public params: ICaptureParameters;

  public domain: string = 'eu';

  public jurisdictions: string[] = [
    'eu-Parliament', 'council', 'eu-council', 'eu-commission', 'eu-Committee of the Regions'
  ];

  protected dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = [['title', 'annex'], 'chapter','section','article'];

  public beforeConvertHook(doc: IDocument): Promise<IDocument> {
    return Promise.resolve(doc);
  }

  public initialCrawl(jurisdictions?: string[]): Promise<any[] | void>{
    global.ARACHNO.logger.info({ "message": 'Performing eurlex initial crawl' });
    return new Promise(async(resolve, reject) => {
      const bases = [
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-parliament-regulations&type=named&qid=1606465815851&DTS_SUBDOM=LEGISLATION', // 2.3k
        'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-parliament-directives&type=named&qid=1608128466243&DTS_SUBDOM=LEGISLATION', // 1.8k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-parliament-decisions&type=named&qid=1608128549439', // 3.2k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Acouncil-regulations&type=named&qid=1606667952270&DTS_SUBDOM=LEGISLATION', // 20k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Acouncil-directives&type=named&qid=1608128673958', // 5k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Acouncil-decisions&type=named&qid=1608128622604&DTS_SUBDOM=LEGISLATION', // 32k
        'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-council&type=named&qid=1606668018591&DTS_SUBDOM=LEGISLATION', // 60
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-commission-regulations&type=named&qid=1606668061120&DTS_SUBDOM=LEGISLATION', // 130k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-commission-directives&type=named&qid=1608128750359&DTS_SUBDOM=LEGISLATION', // 2k
        // 'https://eur-lex.europa.eu/search.html?name=browse-by%3Aeu-commission-decisions&type=named&qid=1608128812717&DTS_SUBDOM=LEGISLATION', // 160k
        'https://eur-lex.europa.eu/search.html?name=browse-by%3Aregions&type=named&qid=1606668125675&DTS_SUBDOM=LEGISLATION&FM_CODED=DEC', // 16
      ];

      const linksArray = [];
      const page = await this.getNewPage();

      for (const baseUrl of bases) {
        try {
          await page.goto(`${baseUrl}`);

          let hasNextPage = true;
          // let i=0;
          while (hasNextPage) {
            // i++;
            const links = await page.$$('css=div.SearchResult>h2>a');

            for(const link of links) {
              const href = await link.getAttribute('href');
              linksArray.push(`https://eur-lex.europa.eu${href.substr(1)}`);
            }

            const nextPageLink = await page.$('a[title="Next Page"]');

            hasNextPage = !!nextPageLink;
            if (nextPageLink) {
              await (new Promise((res) => setTimeout(res, 8000)));
              const nextLink = await nextPageLink.getAttribute('href');
              // lets wait for 5sec before going to next link
              await new Promise((res) => setTimeout(res, 9000));

              await page.goto(`https://eur-lex.europa.eu${nextLink.substr(1)}`);
            }
            // if(i>3) break;
            global.ARACHNO.logger.info({"message": `Links Array length: ${linksArray.length}` });
          }
        }
        catch(e) {
          global.ARACHNO.logger.error({"message": e});
          reject(e);
        }
      }
      await page.close();
      resolve(linksArray);
    });
  }
  public getMetaData(url: string): Promise<IWorkMetaData | void> {
    return new Promise(async (resolve, reject) => {
      try {
        let meta: IWorkMetaData;
        const page = await this.getNewPage();
        await page.goto(url);
        url = decodeURIComponent(url);
        let uniqueId = url.substr(url.indexOf('uri=') + 4);
        uniqueId = uniqueId.split('&')[0];

        let eli:any = await page.$('[title="Gives access to this document through its ELI URI."]');
        if (eli) {
          eli = await eli.getAttribute('href');
          uniqueId = eli.substr(eli.indexOf('europa.eu') + 10);
        }
        let engTitle:any = await page.$('#englishTitle');
        let title:any = await page.$('#title');
        if (title) {
          title = await title.innerText();
          title = title.replace('Consolidated text: ', '');
        } else {
          title = null;
        }

        if (engTitle) {
          engTitle = await engTitle.innerText();
          // in case the title and english title are same, remove the title translation
          // engTitle = engTitle.trim() !== title.trim() ? engTitle : null;
        } else {
          engTitle = null
        }

        let effDate:any = await page.$('meta[property="eli:first_date_entry_in_force"]');
        if (effDate) {
          effDate = new Date(await effDate.getAttribute('content'));
        }

        let workDate:any = await page.$('meta[property="eli:date_publication"]');

        if (workDate) {
          workDate = new Date(await workDate.getAttribute('content'));
        } else {
          let date: any = await page.$('.hd-date, .oj-hd-date')
          if (date) {
            date = await date.innerText();
            date = date.replace('&nbsp;', ' ').trim().match(/(\d+)/g);
            workDate = new Date(`${date[2]}.${date[1]}.${date[0]}`);
          } else {
            workDate = new Date();
          }
        }
        meta = {
          title, // title of work
          language_code: 'eng',
          primary_location_id: 42312,
          source_unique_id: this.domain + '/' + uniqueId,
          source_id: 7,
          start_url: url,
          effective_date: effDate || workDate,
          content_hash: null, // Will be set in Capture class
          title_translation: engTitle || null,
          work_number: null,
          publication_number: null,
          publication_document_number: null,
          issuing_authority_id: null,
          work_type: this.getWorkType(title), // Will be set in Capture class
          work_date: workDate,

          sourceSlug: this.sourceSlug,
        };

        resolve(meta);
      } catch (e) {
        reject(e);
      }
    });
  }

  public getWorkType(title: string) {
    if (title.match(/regulation/gim)) {
      return 'regulation';
    }

    if (title.match(/directive/gim)) {
      return 'directive';
    }

    if (title.match(/decision/gim)) {
      return 'decision';
    }

    return null;
  }

  public beforeEmogrifyHook(doc: String): Promise<String>{
    return Promise.resolve(doc);
  }
  
  public getDoc(url: string): Promise<IDocument | void> {
    global.ARACHNO.logger.info(`Getting doc at: ${url}`);
    
    const doc = new Document(this.sourceSlug);
    return new Promise(async(resolve, reject) => {
      try {
        const page = await this.getNewPage();
        await page.goto(url);

        let documentElement = await page.$('css=#document1');

        if (!documentElement) return reject("Document not found!");

        await this.getPageData(page, {}, true);

        documentElement = await page.$('css=#document1');
        doc.doc = await documentElement.innerHTML();
        doc.mimeType = 'text/html';
        doc.extension = 'html';
        doc.generateHash();
        global.ARACHNO.logger.info(`Captured new doc: ${doc.name}`);
        resolve(doc);
      }
      catch(e) {
        reject(e);
      }
    });
  }

  public getUpdates(jurisdiction?: string[]): Promise<string[] | void> {
    return this.initialCrawl();
  }
  
  public toLegalDoc(doc: IDocument): Promise<IDocument> {
    const levelIdentifiers = [
      {
        type: 'title',
        regex: /(\s*Title\s[IVXLCDM]+)(?:<\/span>)?$/gim,
      },
      {
        type: 'chapter',
        regex: /(\s*Chapter\s[IVXLCDM]+)(?:<\/span>)?$/gim,
      },
      {
        type: 'section',
        regex: /(\s*Section\s+\d+)(?:<\/span>)?$/gim,
      },
      {
        type: 'article',
        regex: /(\s*Article\s+\d+[a-zA-Z]?)(?:<\/span>)?$/gim,
        isSection: true,
      },
      {
        type: 'annex',
        regex: /(\s*ANNEX\s+[IVXLCDM]+)(?:<\/span>)?$/gm,
      }
    ];
    const dom = (new global.ARACHNO.DOMParser())
      .parseFromString(`<div id="main-arch-content">${doc.doc}</div>`, 'text/html');
    const currentStructure = []; // chapter, section, article

    dom.querySelectorAll('p')
      .forEach((num) => {
        levelIdentifiers.some((item) => {
          if (!num.innerHTML || !item.regex.test(num.innerHTML.replace('&nbsp;', ' ').trim())) {
            return false;
          }

          const lastType = currentStructure[currentStructure.length - 1];

          if (lastType !== item.type && !currentStructure.includes(item.type)) {
            currentStructure.push(item.type);
          }

          const id = this.generateId();
          const heading = num.nextElementSibling;

          num.innerHTML = `<span>${num.innerHTML}</span>`;
          const numSpan = num.querySelector(`span`);

          numSpan.setAttribute('data-type', item.type);
          numSpan.setAttribute('data-inline', `num`);
          numSpan.setAttribute('data-id', `${id}`);

          if (item.isSection) {
            numSpan.setAttribute('data-is_section', '1');
          }

          if (heading) {
            heading.innerHTML = `<span>${heading.innerHTML}</span>`;
            const headingSpan = heading.querySelector('span');

            headingSpan.setAttribute('data-inline', `heading`);
            headingSpan.setAttribute('data-id', `${id}`);
          }

          return true;
        });
    });

    doc.doc = dom.querySelector('#main-arch-content').innerHTML;

    return Promise.resolve(doc);
  }
}


// Tasks in librarian not marked as done/updated
// PDFs - where we are
// Where we are on Ireland
// 

// Answer Lara Quickly on the timelines and ALL questions asked