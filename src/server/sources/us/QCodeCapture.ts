import CaptureBase from '@app/modules/CaptureBase';
import IDocument from '@app/interfaces/IDocument';
import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import Document from '@app/modules/Document';
import cuid from 'cuid';
import { dataTypes } from '@app/interfaces/ICapture';



export default class QCodeCapture extends CaptureBase {

  public domain = 'us';
  public sourceSlug = 'us-qcode';
  public jurisdictions = ['ca', 'mi', 'co', 'or', 'mt', 'hi', 'wy'];
  baseUrl: string;

  protected dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = null;

  public async beforeEmogrifyHook(doc: String): Promise<String>{
    return doc;
  }
  
  constructor(params: any) {
    super(params);
    this.baseUrl = 'https://qcode.us';
  }

  public async initialCrawl(jurisdictions?: string[]): Promise<void | any[]> {
    console.log('Perfoming USA QCode Initial Crawl');
    const linksArr = [];
    try {
      const page = await this.getNewPage();
      await page.goto(`${this.baseUrl}/codes.html`);

      const pageCodesLinks = await page.$$(
        'css=div.qcp-publication-list>p>a[href$="?view=desktop"]'
      );
      for (const pageCodeLink of pageCodesLinks) {
        const pTxt = await pageCodeLink.textContent();
        let pageUrl = await pageCodeLink.getAttribute('href');
        pageUrl = pageUrl.replace('http', 'https');

        if (jurisdictions?.length > 0) {
          const pageJurisdiction = pTxt.slice(pTxt.indexOf(',') + 2, pTxt.indexOf(',') + 4);
          if (jurisdictions.includes(pageJurisdiction.toLowerCase())) {
            linksArr.push(pageUrl);
          }
          continue;
        } else linksArr.push(pageUrl);
      }
      await page.close();

      return linksArr;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error in QCode Initial Crawl: ${error}`);
    }
  }

  public async getDoc(url: string): Promise<IDocument | void> {
    const doc = new Document(this.sourceSlug);
    let htmlDoc = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>###</title>
        <link rel="stylesheet" href="https://qcode.us/codes/view.css">
    </head>
    <body>`;
    let pagesStack = [];
    try {
      const page = await this.getNewPage();
      await page.goto(url);

      // This site has frames on this page
      const rightFrame = page.frames().find((frame) => frame.name() === 'RIGHT');
      const mainTitleNode = await rightFrame.$('css=span.navMainTitle');
      if (mainTitleNode) {
        const title = await mainTitleNode.textContent();
        htmlDoc += `<h1 class="currentTopic">${title.trim()}</h1>`;
        htmlDoc = htmlDoc.replace('###', title.trim());
      }
      const firstPageLinks = await rightFrame.$$('css=div.navChildren>div.navChild>a.navChild');
      for (const firstPageLink of firstPageLinks) {
        const nextPagePartUrl = await firstPageLink.getAttribute('href');
        const nextPageUrl = url.replace('?view=desktop', nextPagePartUrl);
        pagesStack.splice(0, 0, nextPageUrl);
      }
      await page.close();
      let count = 0;
      while (pagesStack.length > 0) {
        const currentPageUrl = pagesStack.pop();
        const docPage = await this.getNewPage();
        await docPage.goto(currentPageUrl);

        const showAllExists = await docPage.$('css=a.showAll');

        if (showAllExists !== null) {
          await showAllExists.click();
          const content = await docPage.$('css=div.currentTopic');
          if (content) {
            const contentTxt = await content.textContent();
            const headingTxt = `<h3 class="currentTopic">${contentTxt}</h3>`;
            htmlDoc += headingTxt;
            const docContent = await docPage.$('css=div.navChildren');
            if (docContent) {
              const docContentTxt = await docContent.innerHTML();
              htmlDoc += docContentTxt;
            }
          } else console.log('Expected page has not been found');
        } else {
          const firstPageLinks = await docPage.$$('css=div.navChildren>div.navChild>a.navChild');
          const currentTopic = await docPage.$('css=div.currentTopic');
          if (currentTopic) {
            const currentTopicTxt = await currentTopic.textContent();
            const headingTxt = `<h2 class="currentTopic">${currentTopicTxt}</h2>`;
            htmlDoc += headingTxt;
          }
          const newLinks = [];
          for (const firstPageLink of firstPageLinks) {
            const nextPagePartUrl = await firstPageLink.getAttribute('href');
            const nextPageUrl = url.replace('?view=desktop', nextPagePartUrl);
            newLinks.push(nextPageUrl);
          }
          pagesStack.push(...newLinks.reverse());
          console.log(`Added new links. Current size of the pagesStack is ${pagesStack.length}`);
        }
        await docPage.close();
        if (count == 5) break;
        count++;
      }
      htmlDoc += `</body></html>`;
      htmlDoc = htmlDoc.replace(/\s{2,}/gim, ' ');
      doc.doc = htmlDoc;
      doc.mimeType = 'text/html';
      doc.extension = 'html';
      doc.generateHash();
      return doc;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error getting a document from ${url}: ${error}`);
    }
  }

  public async getMetaData(url: string): Promise<void | IWorkMetaData> {
    console.log(`Get metadata for ${url}`);
    const meta = {
      language_code: 'en',
      primary_location_id: 21, // TODO: update
      source_unique_id: null,
      source_id: 1, // TODO: update
      issuing_authority_id: 44, // TODO: update
      start_url: url, // TODO: update
      title: null,
      effective_date: new Date(),
      content_hash: null, // Will be set in Capture class
      title_translation: null,
      work_number: null,
      publication_number: null,
      publication_document_number: null,
      work_type: null,
      work_date: new Date(),
      sourceSlug: null,
    };

    try {
      const page = await this.getNewPage();
      await page.goto(url);

      const rightFrame = page.frames().find((frame) => frame.name() === 'RIGHT');
      const mainTitleNode = await rightFrame.$('css=span.navMainTitle');
      if (mainTitleNode) {
        const title = await mainTitleNode.textContent();
        meta.title = title.trim();
      }
      return meta;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error getting the USA QCode metadata for ${url}: ${error}`);
    }
  }

  public getUpdates(jurisdiction?: string[]): Promise<void | string[]> {
    throw new Error('Method not implemented.');
  }

  /**
   * Pipeline function invoked before toLegalDoc() -
   * Preprocessing code goes here
   */
  public async beforeConvertHook(doc: IDocument): Promise<IDocument> {
    return doc;
  }
  /**
   * Pipeline function to enrich html document with legalDoc properties and attributes
   * Add source specific tagging code here
   * @param doc the HTML Document
   */
  public async toLegalDoc(doc: IDocument): Promise<IDocument> {
    const levelIdentifier = [
      {
        level: 'title',
        regex: /(\s*Title\s[IVXLCDM]+)(.+)/gim,
      },
      {
        level: 'chapter',
        regex: /(\s*Chapter\s+\d+)(.+)/gim,
      },
      {
        level: 'article',
        regex: /(\s*Article\s+\d+\.)(.+)/gim,
      },
      {
        level: 'section',
        regex: /(Sec.\s\d+[A-Z]?-(?:\d+.)+)(.+)/gim,
        isSection: true,
      },
    ];

    const dom = new global.ARACHNO.DOMParser().parseFromString(doc.doc, 'text/html');

    const tocHeadings = dom.querySelectorAll('.currentTopic, .navTocHeading');

    for (let i = 0; i < tocHeadings.length; i++) {
      const tocHeading = tocHeadings[i];
      levelIdentifier.forEach((item, idx) => {
        if (item.regex.test(tocHeading.innerHTML)) {
          const id = cuid();
          tocHeading.innerHTML = tocHeading.innerHTML.replace(
            item.regex,
            `<span data-inline="num" data-id="${id}">$1 </span><span data-inline="heading" data-id="${id}">$2</span>`
          );
          tocHeading.setAttribute('data-type', item.level);
          if (item.isSection) {
            tocHeading.setAttribute('data-is_section', '1');
          }
          tocHeading.setAttribute('data-level', `${idx + 1}`);
          // tocHeading.setAttribute('data-indent', '');
          // tocHeading.setAttribute('data-id', cuid());
        }
      });
    }
    doc.doc = dom.documentElement.outerHTML;

    return doc;
  }
  
}
