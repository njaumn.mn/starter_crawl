import CaptureBase from '@app/modules/CaptureBase';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import IDocument from '@app/interfaces/IDocument';
import Document from '@app/modules/Document';
import { dataTypes } from '@app/interfaces/ICapture';

export default class FloridaLegInfoCapture extends CaptureBase {
  public toLegalDoc(doc: IDocument): Promise<IDocument> {
    throw new Error('Method not implemented.');
  }
  private baseDomain: string;

  constructor(params: any) {
    super(params);
    this.baseDomain = 'http://www.leg.state.fl.us';
  }

  public sourceSlug = 'us-flleginfo';
  public params: ICaptureParameters;

  public domain = 'us'; //United States

  public jurisdictions = ['us-fl'];

  protected dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = null;

  public async beforeEmogrifyHook(doc: String): Promise<String> {
    return doc;
  }

  public async beforeConvertHook(doc: IDocument): Promise<IDocument> {
    return doc;
  }

  public async initialCrawl(jurisdictions?: string[]): Promise<string[]> {
    const codesBaseUrl = `${this.baseDomain}/Statutes/index.cfm?Mode=View%20Statutes&Submenu=1&Tab=statutes&CFID=260720230&CFTOKEN=23950736`;
    // const linksArr = [];
    try {
      // const page = await this.getNewPage();
      // await page.goto(codesBaseUrl);
      return [codesBaseUrl];
    } catch (error) {
      return Promise.reject(`Error performing the initialCrawl on ${codesBaseUrl}: ${error}`);
    }
    // console.log(linksArr);
  }


  public async getDoc(url: string): Promise<IDocument> {
    const urlObj = new URL(url);
    const tocCode = urlObj.searchParams.get('tocCode');
    const expandedTocUrl = `${this.baseDomain}/faces/codedisplayexpand.xhtml?tocCode=${tocCode}`;

    // const links = await page.$$('table>tbody>tr>td>a');
    //   for (const link of links) {
    //     const linkHrefVal = await link.getAttribute('href');
    //     const linkFullUrl = `${this.baseDomain}${linkHrefVal}`;
    //     linksArr.push(linkFullUrl);
    //   }

    try {
      const page = await this.getNewPage();
      await page.goto(expandedTocUrl);
      const tocPage = await page.$('css=div[id="expandedbranchcodesid"]');
      const pageUrls = [];
      if (tocPage) {
        const tocPageUrls = await tocPage.$$('css=div>a[style="cursor: pointer;"]');
        for (const tocPageUrl of tocPageUrls) {
          const pageTitleDiv = await tocPageUrl.$('css=div:first-child');
          const pageTitle = await pageTitleDiv.textContent();
          const pageTitleTrim = pageTitle.trim();
          const matches = pageTitleTrim.match(/^(DIVISION|TITLE|PART|CHAPTER|ARTICLE)/gm);
          if (matches) {
            const matchTxt = matches.pop();
            // 1. If the next heading is the same, save that.
            // 2. If different, skip and do step 1
            const idx = tocPageUrls.indexOf(tocPageUrl);
            const nextPageTitle = tocPageUrls.slice(idx, idx + 1);
            const nextPageTitleTxt = await nextPageTitle.pop().textContent();
            if (nextPageTitleTxt.trim().match(matchTxt)) {
              const pageUrl = await tocPageUrl.getAttribute('href');
              if (pageUrl) {
                const fullPageUrl = `${this.baseDomain}${pageUrl}`;
                pageUrls.push(fullPageUrl);
              }
            }
          } else {
            // gotta be uppercase
            const ignore = ['COMMERCIAL CODE']; // these TOC items have no data
            if (pageTitleTrim === pageTitleTrim.toUpperCase() && !ignore.includes(pageTitleTrim)) {
              const pageUrl = await tocPageUrl.getAttribute('href');
              if (pageUrl) {
                const fullPageUrl = `${this.baseDomain}${pageUrl}`;
                pageUrls.push(fullPageUrl);
              }
            }
          }
        }

        // Stitch all urls into a document string
        const documentObj = new Document(this.sourceSlug);
        let documentString = '';

        // Asynchronous version
        // const entirePageContent = await Promise.all(
        //   pageUrls.map(async (pageUrl) => {
        //     try {
        //       const docPage = await browserCtx.newPage();
        //       await docPage.goto(pageUrl);
        //       let pageContentTxt = '';
        //       const pageContent = await docPage.$('css=div#manylawsections>div:not(:nth-child(1))');
        //       if (pageContent) {
        //         pageContentTxt = await pageContent.innerHTML();
        //         await docPage.close();
        //       }
        //       return pageContentTxt;
        //     } catch (error) {
        //       return Promise.reject(`Error fetching ${pageUrl}: ${error}`);
        //     }
        //   })
        // );

        // entirePageContent.forEach((pageContent) => {
        //   documentString += pageContent;
        // });

        // Synchronous version
        for (const pageUrl of pageUrls) {
          const docPage = await this.getNewPage();
          try {
            await docPage.goto(pageUrl);

            const pageContent = await docPage.$$('css=div#manylawsections>div:not(:nth-child(1))');
            if (pageContent) {
              for (const content of pageContent) {
                documentString += await content.innerHTML();
              }
            }
          } catch (error) {
            console.error(error);
            return Promise.reject(`Failed to get content from ${pageUrl}: ${error}`);
          } finally {
            docPage.close();
          }
        }

        const fs = require('fs');
        // write data to file sample.html
        fs.writeFile(
          'doc.html',
          documentString,
          // callback function that is called after writing file is done
          (err: any) => {
            if (err) throw err;
            // if no error
            console.log('Data is written to file successfully.');
          }
        );

        documentObj.doc = documentString;
        documentObj.mimeType = 'text/html';
        documentObj.extension = 'html';
        documentObj.generateHash();
        return Promise.resolve(documentObj);
      } else return Promise.reject(`The url ${expandedTocUrl} does not have a table of contents`);
    } catch (error) {
      return Promise.reject(error);
    }
  }
  public async getMetaData(url: string): Promise<IWorkMetaData> {
    const urlObj = new URL(url);
    const tocTitle = urlObj.searchParams.get('tocTitle');
    const tocCode = urlObj.searchParams.get('tocCode');
    const meta = {
      title: tocTitle,
      language_code: 'en',
      primary_location_id: 21,
      source_unique_id: '11',
      source_id: 1,
      start_url: url,
      effective_date: new Date(),
      content_hash: null, // Will be set in Capture class
      title_translation: '',
      work_number: tocCode,
      publication_number: '',
      publication_document_number: '',
      issuing_authority_id: 44,
      work_type: null, // Will be set in Capture class
      work_date: new Date(),
      sourceSlug: this.sourceSlug,
    };
    return meta;
  }
  public getUpdates(): Promise<string[]> {
    return Promise.resolve(['']);
  }
}
