"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var CaptureBase_1 = require("@app/modules/CaptureBase");
var Document_1 = require("@app/modules/Document");
var FloridaLegInfoCapture = /** @class */ (function (_super) {
    __extends(FloridaLegInfoCapture, _super);
    function FloridaLegInfoCapture(params) {
        var _this = _super.call(this, params) || this;
        _this.sourceSlug = 'us-flleginfo';
        _this.domain = 'us'; //United States
        _this.jurisdictions = ['us-fl'];
        _this.dataTypesHierarchy = null;
        _this.baseDomain = 'http://www.leg.state.fl.us';
        return _this;
    }
    FloridaLegInfoCapture.prototype.toLegalDoc = function (doc) {
        throw new Error('Method not implemented.');
    };
    FloridaLegInfoCapture.prototype.beforeEmogrifyHook = function (doc) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, doc];
            });
        });
    };
    FloridaLegInfoCapture.prototype.beforeConvertHook = function (doc) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, doc];
            });
        });
    };
    FloridaLegInfoCapture.prototype.initialCrawl = function (jurisdictions) {
        return __awaiter(this, void 0, void 0, function () {
            var codesBaseUrl, linksArr, page, links, _i, links_1, link, linkHrefVal, linkFullUrl, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        codesBaseUrl = this.baseDomain + "/Statutes/index.cfm?Mode=View%20Statutes&Submenu=1&Tab=statutes&CFID=260720230&CFTOKEN=23950736";
                        linksArr = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 9, , 10]);
                        return [4 /*yield*/, this.getNewPage()];
                    case 2:
                        page = _a.sent();
                        return [4 /*yield*/, page.goto(codesBaseUrl)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, page.$$('table>tbody>tr>td>a')];
                    case 4:
                        links = _a.sent();
                        _i = 0, links_1 = links;
                        _a.label = 5;
                    case 5:
                        if (!(_i < links_1.length)) return [3 /*break*/, 8];
                        link = links_1[_i];
                        return [4 /*yield*/, link.getAttribute('href')];
                    case 6:
                        linkHrefVal = _a.sent();
                        linkFullUrl = "" + this.baseDomain + linkHrefVal;
                        linksArr.push(linkFullUrl);
                        _a.label = 7;
                    case 7:
                        _i++;
                        return [3 /*break*/, 5];
                    case 8: return [2 /*return*/, linksArr];
                    case 9:
                        error_1 = _a.sent();
                        return [2 /*return*/, Promise.reject("Error performing the initialCrawl on " + codesBaseUrl + ": " + error_1)];
                    case 10:
                        console.log(linksArr);
                        return [2 /*return*/];
                }
            });
        });
    };
    FloridaLegInfoCapture.prototype.getFinalPageLinks = function (startUrl) {
        return __awaiter(this, void 0, void 0, function () {
            var pageUrls, page, tocPage, tocPageUrls, _i, tocPageUrls_1, tocPageUrl, pageTitleDiv, pageTitle, pageTitleTrim, matches, matchTxt, idx, secondUrl, secondUrlTxt, pageUrl, fullPageUrl, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pageUrls = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 13, , 14]);
                        return [4 /*yield*/, this.getNewPage()];
                    case 2:
                        page = _a.sent();
                        return [4 /*yield*/, page.goto(startUrl)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, page.$('css=div[id="expandedbranchcodesid"]')];
                    case 4:
                        tocPage = _a.sent();
                        if (!tocPage) return [3 /*break*/, 12];
                        return [4 /*yield*/, tocPage.$$('css=div>a[style="cursor: pointer;"]')];
                    case 5:
                        tocPageUrls = _a.sent();
                        _i = 0, tocPageUrls_1 = tocPageUrls;
                        _a.label = 6;
                    case 6:
                        if (!(_i < tocPageUrls_1.length)) return [3 /*break*/, 12];
                        tocPageUrl = tocPageUrls_1[_i];
                        return [4 /*yield*/, tocPageUrl.$('css=div:first-child')];
                    case 7:
                        pageTitleDiv = _a.sent();
                        return [4 /*yield*/, pageTitleDiv.textContent()];
                    case 8:
                        pageTitle = _a.sent();
                        pageTitleTrim = pageTitle.trim();
                        matches = pageTitleTrim.match(/^(DIVISION|TITLE|PART|CHAPTER|ARTICLE)/gm);
                        if (!matches) return [3 /*break*/, 11];
                        matchTxt = matches.pop();
                        idx = tocPageUrls.indexOf(tocPageUrl);
                        secondUrl = tocPageUrls.slice(idx, idx + 1);
                        return [4 /*yield*/, secondUrl.pop().textContent()];
                    case 9:
                        secondUrlTxt = _a.sent();
                        if (!secondUrlTxt.trim().match(matchTxt)) return [3 /*break*/, 11];
                        return [4 /*yield*/, tocPageUrl.getAttribute('href')];
                    case 10:
                        pageUrl = _a.sent();
                        if (pageUrl) {
                            fullPageUrl = "" + this.baseDomain + pageUrl;
                            pageUrls.push(fullPageUrl);
                            console.log(fullPageUrl);
                        }
                        _a.label = 11;
                    case 11:
                        _i++;
                        return [3 /*break*/, 6];
                    case 12: return [2 /*return*/, pageUrls];
                    case 13:
                        error_2 = _a.sent();
                        return [2 /*return*/, Promise.reject("Error getting final page urls for " + startUrl + ": " + error_2)];
                    case 14: return [2 /*return*/];
                }
            });
        });
    };
    FloridaLegInfoCapture.prototype.getTOCLinks = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var pageUrls, page, tocPage, tocPageUrls, _i, tocPageUrls_2, tocPageUrl, pageTitleDiv, pageTitle, pageTitleTrim, matches, matchTxt, idx, secondUrl, secondUrlTxt, pageUrl, fullPageUrl, finalPageLinks, ignore, pageUrl, fullPageUrl, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pageUrls = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 17, , 18]);
                        return [4 /*yield*/, this.getNewPage()];
                    case 2:
                        page = _a.sent();
                        return [4 /*yield*/, page.goto(url)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, page.$('css=div[id="expandedbranchcodesid"]')];
                    case 4:
                        tocPage = _a.sent();
                        if (!tocPage) return [3 /*break*/, 16];
                        return [4 /*yield*/, tocPage.$$('css=div>a[style="cursor: pointer;"]')];
                    case 5:
                        tocPageUrls = _a.sent();
                        _i = 0, tocPageUrls_2 = tocPageUrls;
                        _a.label = 6;
                    case 6:
                        if (!(_i < tocPageUrls_2.length)) return [3 /*break*/, 16];
                        tocPageUrl = tocPageUrls_2[_i];
                        return [4 /*yield*/, tocPageUrl.$('css=div:first-child')];
                    case 7:
                        pageTitleDiv = _a.sent();
                        return [4 /*yield*/, pageTitleDiv.textContent()];
                    case 8:
                        pageTitle = _a.sent();
                        pageTitleTrim = pageTitle.trim();
                        matches = pageTitleTrim.match(/^(DIVISION|TITLE|PART|CHAPTER|ARTICLE)/gm);
                        if (!matches) return [3 /*break*/, 13];
                        matchTxt = matches.pop();
                        idx = tocPageUrls.indexOf(tocPageUrl);
                        secondUrl = tocPageUrls.slice(idx, idx + 1);
                        return [4 /*yield*/, secondUrl.pop().textContent()];
                    case 9:
                        secondUrlTxt = _a.sent();
                        if (!(secondUrlTxt.trim().match(matchTxt) && secondUrlTxt.trim() !== 'ARTICLE')) return [3 /*break*/, 12];
                        return [4 /*yield*/, tocPageUrl.getAttribute('href')];
                    case 10:
                        pageUrl = _a.sent();
                        if (!pageUrl) return [3 /*break*/, 12];
                        fullPageUrl = "" + this.baseDomain + pageUrl;
                        return [4 /*yield*/, this.getFinalPageLinks(fullPageUrl)];
                    case 11:
                        finalPageLinks = _a.sent();
                        pageUrls.push.apply(pageUrls, finalPageLinks);
                        _a.label = 12;
                    case 12: return [3 /*break*/, 15];
                    case 13:
                        ignore = ['COMMERCIAL CODE'];
                        if (!(pageTitleTrim === pageTitleTrim.toUpperCase() && !ignore.includes(pageTitleTrim))) return [3 /*break*/, 15];
                        return [4 /*yield*/, tocPageUrl.getAttribute('href')];
                    case 14:
                        pageUrl = _a.sent();
                        if (pageUrl) {
                            fullPageUrl = "" + this.baseDomain + pageUrl;
                            pageUrls.push(fullPageUrl);
                        }
                        _a.label = 15;
                    case 15:
                        _i++;
                        return [3 /*break*/, 6];
                    case 16: return [2 /*return*/, pageUrls];
                    case 17:
                        err_1 = _a.sent();
                        return [2 /*return*/, Promise.reject("Error getting TOC links for " + url + ": " + err_1)];
                    case 18: return [2 /*return*/];
                }
            });
        });
    };
    FloridaLegInfoCapture.prototype.getDoc = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var urlObj, tocCode, expandedTocUrl, page, tocPage, pageUrls, tocPageUrls, _i, tocPageUrls_3, tocPageUrl, pageTitleDiv, pageTitle, pageTitleTrim, matches, matchTxt, idx, nextPageTitle, nextPageTitleTxt, pageUrl, fullPageUrl, ignore, pageUrl, fullPageUrl, documentObj, documentString, _a, pageUrls_1, pageUrl, docPage, pageContent, _b, pageContent_1, content, _c, error_3, fs, error_4;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        urlObj = new URL(url);
                        tocCode = urlObj.searchParams.get('tocCode');
                        expandedTocUrl = this.baseDomain + "/faces/codedisplayexpand.xhtml?tocCode=" + tocCode;
                        _d.label = 1;
                    case 1:
                        _d.trys.push([1, 31, , 32]);
                        return [4 /*yield*/, this.getNewPage()];
                    case 2:
                        page = _d.sent();
                        return [4 /*yield*/, page.goto(expandedTocUrl)];
                    case 3:
                        _d.sent();
                        return [4 /*yield*/, page.$('css=div[id="expandedbranchcodesid"]')];
                    case 4:
                        tocPage = _d.sent();
                        pageUrls = [];
                        if (!tocPage) return [3 /*break*/, 29];
                        return [4 /*yield*/, tocPage.$$('css=div>a[style="cursor: pointer;"]')];
                    case 5:
                        tocPageUrls = _d.sent();
                        _i = 0, tocPageUrls_3 = tocPageUrls;
                        _d.label = 6;
                    case 6:
                        if (!(_i < tocPageUrls_3.length)) return [3 /*break*/, 15];
                        tocPageUrl = tocPageUrls_3[_i];
                        return [4 /*yield*/, tocPageUrl.$('css=div:first-child')];
                    case 7:
                        pageTitleDiv = _d.sent();
                        return [4 /*yield*/, pageTitleDiv.textContent()];
                    case 8:
                        pageTitle = _d.sent();
                        pageTitleTrim = pageTitle.trim();
                        matches = pageTitleTrim.match(/^(DIVISION|TITLE|PART|CHAPTER|ARTICLE)/gm);
                        if (!matches) return [3 /*break*/, 12];
                        matchTxt = matches.pop();
                        idx = tocPageUrls.indexOf(tocPageUrl);
                        nextPageTitle = tocPageUrls.slice(idx, idx + 1);
                        return [4 /*yield*/, nextPageTitle.pop().textContent()];
                    case 9:
                        nextPageTitleTxt = _d.sent();
                        if (!nextPageTitleTxt.trim().match(matchTxt)) return [3 /*break*/, 11];
                        return [4 /*yield*/, tocPageUrl.getAttribute('href')];
                    case 10:
                        pageUrl = _d.sent();
                        if (pageUrl) {
                            fullPageUrl = "" + this.baseDomain + pageUrl;
                            pageUrls.push(fullPageUrl);
                        }
                        _d.label = 11;
                    case 11: return [3 /*break*/, 14];
                    case 12:
                        ignore = ['COMMERCIAL CODE'];
                        if (!(pageTitleTrim === pageTitleTrim.toUpperCase() && !ignore.includes(pageTitleTrim))) return [3 /*break*/, 14];
                        return [4 /*yield*/, tocPageUrl.getAttribute('href')];
                    case 13:
                        pageUrl = _d.sent();
                        if (pageUrl) {
                            fullPageUrl = "" + this.baseDomain + pageUrl;
                            pageUrls.push(fullPageUrl);
                        }
                        _d.label = 14;
                    case 14:
                        _i++;
                        return [3 /*break*/, 6];
                    case 15:
                        documentObj = new Document_1["default"](this.sourceSlug);
                        documentString = '';
                        _a = 0, pageUrls_1 = pageUrls;
                        _d.label = 16;
                    case 16:
                        if (!(_a < pageUrls_1.length)) return [3 /*break*/, 28];
                        pageUrl = pageUrls_1[_a];
                        return [4 /*yield*/, this.getNewPage()];
                    case 17:
                        docPage = _d.sent();
                        _d.label = 18;
                    case 18:
                        _d.trys.push([18, 25, 26, 27]);
                        return [4 /*yield*/, docPage.goto(pageUrl)];
                    case 19:
                        _d.sent();
                        return [4 /*yield*/, docPage.$$('css=div#manylawsections>div:not(:nth-child(1))')];
                    case 20:
                        pageContent = _d.sent();
                        if (!pageContent) return [3 /*break*/, 24];
                        _b = 0, pageContent_1 = pageContent;
                        _d.label = 21;
                    case 21:
                        if (!(_b < pageContent_1.length)) return [3 /*break*/, 24];
                        content = pageContent_1[_b];
                        _c = documentString;
                        return [4 /*yield*/, content.innerHTML()];
                    case 22:
                        documentString = _c + _d.sent();
                        _d.label = 23;
                    case 23:
                        _b++;
                        return [3 /*break*/, 21];
                    case 24: return [3 /*break*/, 27];
                    case 25:
                        error_3 = _d.sent();
                        console.error(error_3);
                        return [2 /*return*/, Promise.reject("Failed to get content from " + pageUrl + ": " + error_3)];
                    case 26:
                        docPage.close();
                        return [7 /*endfinally*/];
                    case 27:
                        _a++;
                        return [3 /*break*/, 16];
                    case 28:
                        fs = require('fs');
                        // write data to file sample.html
                        fs.writeFile('doc.html', documentString, 
                        // callback function that is called after writing file is done
                        function (err) {
                            if (err)
                                throw err;
                            // if no error
                            console.log('Data is written to file successfully.');
                        });
                        documentObj.doc = documentString;
                        documentObj.mimeType = 'text/html';
                        documentObj.extension = 'html';
                        documentObj.generateHash();
                        return [2 /*return*/, Promise.resolve(documentObj)];
                    case 29: return [2 /*return*/, Promise.reject("The url " + expandedTocUrl + " does not have a table of contents")];
                    case 30: return [3 /*break*/, 32];
                    case 31:
                        error_4 = _d.sent();
                        return [2 /*return*/, Promise.reject(error_4)];
                    case 32: return [2 /*return*/];
                }
            });
        });
    };
    FloridaLegInfoCapture.prototype.getMetaData = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var urlObj, tocTitle, tocCode, meta;
            return __generator(this, function (_a) {
                urlObj = new URL(url);
                tocTitle = urlObj.searchParams.get('tocTitle');
                tocCode = urlObj.searchParams.get('tocCode');
                meta = {
                    title: tocTitle,
                    language_code: 'en',
                    primary_location_id: 21,
                    source_unique_id: '11',
                    source_id: 1,
                    start_url: url,
                    effective_date: new Date(),
                    content_hash: null,
                    title_translation: '',
                    work_number: tocCode,
                    publication_number: '',
                    publication_document_number: '',
                    issuing_authority_id: 44,
                    work_type: null,
                    work_date: new Date(),
                    sourceSlug: this.sourceSlug
                };
                return [2 /*return*/, meta];
            });
        });
    };
    FloridaLegInfoCapture.prototype.getUpdates = function () {
        return Promise.resolve(['']);
    };
    return FloridaLegInfoCapture;
}(CaptureBase_1["default"]));
exports["default"] = FloridaLegInfoCapture;
