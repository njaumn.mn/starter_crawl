import CaptureBase from '@app/modules/CaptureBase';
import IDocument from '@app/interfaces/IDocument';
import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import Document from '@app/modules/Document';
import cuid from 'cuid';



export default class MontanaCodeCapture extends CaptureBase {
  protected dataTypesHierarchy: (string | string[])[];
  public beforeEmogrifyHook(doc: String): Promise<String> {
    throw new Error('Method not implemented.');
  }

  public domain = 'us';
  public sourceSlug = 'us-montanacode';
  public jurisdictions = ['mt'];
  baseUrl: string;
  
  constructor(params: any) {
    super(params);
    this.baseUrl = 'https://leg.mt.gov/bills/mca';
  }

  public async initialCrawl(jurisdictions?: string[]): Promise<void | any[]> {
    console.log('Perfoming USA Montana Code Initial Crawl');
    const codesBaseUrl = `${this.baseUrl}/index.html`;
    const linksArr = [];
    try {
      const page = await this.getNewPage();
      await page.goto(codesBaseUrl);

      const pageCodesLinks = await page.$$(
        'css=div.title-toc-content>ul>li>a'
      );
      for (const pageCodeLink of pageCodesLinks) {
        const pTxt = await pageCodeLink.textContent();
        let pageUrl = await pageCodeLink.getAttribute('href');
        pageUrl = this.baseUrl + pageUrl.substring(1);

        if (jurisdictions.length > 0) {
          const pageJurisdiction = pTxt.slice(pTxt.indexOf(',') + 2, pTxt.indexOf(',') + 4);
          if (jurisdictions.includes(pageJurisdiction.toLowerCase())) {
            linksArr.push(pageUrl);
          }
          continue;
        } else linksArr.push(pageUrl);
      }
      await page.close();

      return linksArr;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error in MontanaCode Initial Crawl: ${error}`);
    }
  }

  public async getDoc(url: string): Promise<IDocument | void> {
    const doc = new Document(this.sourceSlug);
    let htmlDoc = `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>###</title>
        <link rel="stylesheet" href="https://leg.mt.gov/bills/mca/css/mt.css">
    </head>
    <body>`;
    let pagesStack = [];
    try {
      const page = await this.getNewPage();
      await page.goto(url);

      const mainTitleNode = await page.$('css=h1.chapter-title-title');
      if (mainTitleNode) {
        const title = await mainTitleNode.textContent();
        htmlDoc += `<h1 class="chapter-title-title">${title.trim()}</h1>`;
        htmlDoc = htmlDoc.replace('###', title.trim());
      }
      const firstPageLinks = await page.$$('css=div.chapter-toc-content>ul>li>a');
      for (const firstPageLink of firstPageLinks) {
        const nextPagePartUrl = await firstPageLink.getAttribute('href');
        const nextPageUrl = url.replace('\/chapters_index.html', nextPagePartUrl.substring(1));
        pagesStack.splice(0, 0, nextPageUrl);
      }
      await page.close();
      //let count = 0;
      while (pagesStack.length > 0) {
        const currentPageUrl = pagesStack.pop();
        const docPage = await this.getNewPage();
        await docPage.goto(currentPageUrl);

        const secondPageLinks = await docPage.$$('css=div.part-toc-content>ul>li>a');
        const chapterTitle = await docPage.$('css=h1.part-chapter-title');
        if (chapterTitle) {
          const chapterTitleTxt = await chapterTitle.textContent();
          const headingTxt = `<h2 class="part-chapter-title">${chapterTitleTxt}</h2>`;
          htmlDoc += headingTxt;
        }
        let newSecondLinks = [];
        for (const secondPageLink of secondPageLinks) {
          let nextPagePartUrl = await secondPageLink.getAttribute('href');
          if (nextPagePartUrl.substring(0,2) === "./" ) {
            nextPagePartUrl = nextPagePartUrl.substring(2)
          }
          const nextPageUrl = currentPageUrl.replace('parts_index.html', nextPagePartUrl);
          newSecondLinks.push(nextPageUrl);
        }
        newSecondLinks = newSecondLinks.reverse()
        while (newSecondLinks.length > 0) {
          const currentPageUrl = newSecondLinks.pop();
          const docPage = await this.getNewPage();
          await docPage.goto(currentPageUrl);
  
          const thirdPageLinks = await docPage.$$('css=div.section-toc-content>ul>li>a');
          const sectionTitle = await docPage.$('css=h1.section-part-title');
          if (sectionTitle) {
            const sectionTitleTxt = await sectionTitle.textContent();
            const headingTxt = `<h3 class="part-chapter-title">${sectionTitleTxt}</h3>`;
            htmlDoc += headingTxt;
          }
          let newThirdLinks = [];
          for (const thirdPageLink of thirdPageLinks) {
            let nextPagePartUrl = await thirdPageLink.getAttribute('href');
            if (nextPagePartUrl.substring(0,2) === "./" ) {
              nextPagePartUrl = nextPagePartUrl.substring(2)
            }
            const nextPageUrl = currentPageUrl.replace('sections_index.html', nextPagePartUrl);
            newThirdLinks.push(nextPageUrl);
          }
          newThirdLinks = newThirdLinks.reverse()
          while (newThirdLinks.length > 0) {
            const currentPageUrl = newThirdLinks.pop();
            const docPage = await this.getNewPage();
            await docPage.goto(currentPageUrl);

            const sectionTitle = await docPage.$('css=h1.section-section-title');
            const sectionNumberTitle = await docPage.$('css=span.catchline');
            if (sectionNumberTitle) {
              const sectionTitleTxt = await sectionNumberTitle.textContent();
              const headingTxt = `<h4 class="section-section-title">${sectionTitleTxt}</h4>`;
              htmlDoc += headingTxt;
            } else if (sectionTitle){
              const sectionTitleTxt = await sectionTitle.textContent();
              const headingTxt = `<h4 class="section-section-title">${sectionTitleTxt}</h4>`;
              htmlDoc += headingTxt;
            }
            const pageContent = await docPage.$$('css=div.section-content>p.line-indent');
            if (pageContent) {
              for (const content of pageContent) {
                htmlDoc += await content.innerHTML();
              }
            }
          }
        }
 
        await docPage.close();
       // if (count == 5) break; 
       //   count++;
      }
      htmlDoc += `</body></html>`;
      htmlDoc = htmlDoc.replace(/\s{2,}/gim, ' ');
      doc.doc = htmlDoc;
      doc.mimeType = 'text/html';
      doc.extension = 'html';
      doc.generateHash();
      return doc;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error getting a document from ${url}: ${error}`);
    }
  }

  public async getMetaData(url: string): Promise<void | IWorkMetaData> {
    console.log(`Get metadata for ${url}`);
    const meta = {
      language_code: 'en',
      primary_location_id: 2360, // location title = 'Montana'
      source_unique_id: null,
      sourceId: 384, // location title = 'United States of America'
      issuing_authority_id: null, 
      startUrl: url, // eg 'https://leg.mt.gov/bills/mca/title_0000/chapters_index.html'
      title: null,
      expression_date: null,
      content_hash: null,
      title_translation: null,
      document_number: null,
      publication_number: null,
      publication_document_number: null,
      work_type: 'constitution', 
      work_date: null,
      sourceSlug: null,
    };

    try {
      // Extract the source unique id
      const urlObj = new URL(url);
      const urlPathName = urlObj.pathname;
      const arr = urlPathName.split('/')
      arr.pop()
      const source_unique_id = arr.join('/');
      if (source_unique_id) {
        meta.source_unique_id = source_unique_id.trim();
      }

      // Extract the title
      const page = await this.getNewPage();
      await page.goto(url);
      const mainTitleNode = await page.$('css=h1.chapter-title-title');
      if (mainTitleNode) {
        const title = await mainTitleNode.textContent();
        meta.title = title.trim();
      }
      // return meta;
    } catch (error) {
      console.error(error);
      return Promise.reject(`Error getting the Montana metadata for ${url}: ${error}`);
    }
  }

  public getUpdates(jurisdiction?: string[]): Promise<void | string[]> {
    throw new Error('Method not implemented.');
  }

  /**
   * Pipeline function invoked before toLegalDoc() -
   * Preprocessing code goes here
   */
  public async beforeConvertHook(doc: IDocument): Promise<IDocument> {
    return doc;
  }
  /**
   * Pipeline function to enrich html document with legalDoc properties and attributes
   * Add source specific tagging code here
   * @param doc the HTML Document
   */
  public async toLegalDoc(doc: IDocument): Promise<IDocument> {
    const levelIdentifier = [
      {
        level: 'title',
        regex: /(\s*TITLE\s\d+)(.+)/gim,
      },
      {
        level: 'chapter',
        regex: /(\s*Chapter\s\d+)(.+)/gim,
      },
      {
        level: 'article',
        regex: /(\s*ARTICLE\s[IVXLCDM]+)(.+)/gim,
      },
      {
        level: 'part',
        regex: /(\s*Part\s[IVXLCDM]+)(.+)/gim,
      },
      {
        level: 'part',
        regex: /(\s*Part\s\d+)(.+)/gim,
      },
      {
        level: 'section',
        regex: /(\s*Section.\d+)(.+)/gim,
        isSection: true,
      },
      {
        level: 'section',
        regex: /(\s*\d+\-\d+\-\d+)(.+)/gim,
        isSection: true,
      },
    ];

    const dom = new global.ARACHNO.DOMParser().parseFromString(doc.doc, 'text/html');

    const tocHeadings = dom.querySelectorAll('.chapter-title-title, .part-chapter-title, .section-section-title');

    for (let i = 0; i < tocHeadings.length; i++) {
      const tocHeading = tocHeadings[i];
      levelIdentifier.forEach((item, idx) => {
        if (item.regex.test(tocHeading.innerHTML)) {
          const id = cuid();
          tocHeading.innerHTML = tocHeading.innerHTML.replace(
            item.regex,
            `<span data-inline="num" data-id="${id}">$1 </span><span data-inline="heading" data-id="${id}">$2</span>`
          );
          tocHeading.setAttribute('data-type', item.level);
          if (item.isSection) {
            tocHeading.setAttribute('data-is_section', '1');
          }
          tocHeading.setAttribute('data-level', `${idx + 1}`);
        }
      });
    }
    doc.doc = dom.documentElement.outerHTML;

    return doc;
  }
  
}
