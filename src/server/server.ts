import express from 'express';
import * as bodyParser from 'body-parser';
import multer from "multer";
import { v4 as uuidv4 } from 'uuid';
import config from './config';
import Capture from '@app/services/Capture';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import DOMLoader from './utils/DOMLoader';
import { 
  initialNewPayloadValidator, 
  initialNewPayloadValidatorCustom,
  capturePayloadValidator,
  defaultPayloadValidatorCustom,
  convertPayloadValidator
} from './validators';
import Store from '@app/services/Store';
import Logger from 'utils/Logger';


const app = express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

DOMLoader.init();
Logger.init();

const port = Number(config.get('port') || 4200);
const captureService = new Capture();
const store = new Store();

app.get('/version', (req, res) => res.json('Arachno scripts service API v1'));

/**
 * Initial Crawl of a source - Captures all works available for a source
 * and converts them to LegalDoc - and saved to corpus|aws bucket
 * @param req.sourceId - source id we want to crawl (sourceSlug)
 */
app.post('/initial-crawl', initialNewPayloadValidator(), initialNewPayloadValidatorCustom, async (req, res) => {
  const captureParams: ICaptureParameters = {
    sourceId: req.body.sourceId ? req.body.sourceId : '',
    domain: req.body.domain ? req.body.domain : '',
    jurisdictions: req.body.jurisdictions ? req.body.jurisdictions : null,
    jobId: uuidv4(),
    jobType: "initialCrawl"
  };

  try {
    captureService.initialCrawl(captureParams);    
    res.status(200).json({ error: null, message: `Initial crawl of ${captureParams.domain || captureParams.jurisdictions || captureParams.sourceId} started successfully`, 
                            jobId: captureParams.jobId});
  } catch (error) {
    console.error(error);
    res.status(400).json({
      error: { message: error.message, errorName: error.name || error.args, error: error },
      message: null,
    });
  }
  
});

/**
 * Metadata extraction for work given source script id to use
 * @param req.sourceId - source id we want to crawl (sourceSlug)
 */
app.post('/get-metadata', initialNewPayloadValidator(), initialNewPayloadValidatorCustom, async (req, res) => {
  const captureParams: ICaptureParameters = {
    sourceId: req.body.sourceId,
    startUrl: req.body.startUrl,
    jobId: uuidv4(),
    jobType: "getMetadata"
  };

  try {
    captureService.getMetadata(captureParams);
    res.status(200).json({ error: null, message: `Metadata extraction started successfully. Using Script: '${captureParams.sourceId}' for work: ${captureParams.startUrl}`, 
                            jobId: captureParams.jobId});
  } catch (error) {
    console.error(error);
    res.status(400).json({
      error: { message: error.message, errorName: error.name || error.args, error: error },
      message: null,
    });
  }
  
});


/**
 * Captures the work at the url
 * @param req.sourceId - source id we want to crawl (sourceSlug)
 * @param req.source_unique_id - unique id of this particular work we are capturing
 * @param req.startUrl - source id we want to crawl
 */
app.post('/get-doc', capturePayloadValidator(), defaultPayloadValidatorCustom, async (req, res) => {
  console.log(req.body);
  const captureParams: ICaptureParameters = {
    sourceId: req.body.sourceId,
    startUrl: req.body.startUrl,
    jobId: uuidv4(),
    jobType: "getDoc"
  };

  captureService.getDoc(captureParams);
  res.status(200).json({ error: null, message: `Capture started for ${captureParams.startUrl}`, jobId: captureParams.jobId });
  
});

/**
 * Checks for new works(updates) - usually located on an updates page on the
 * source identified by sourceId
 * @param req.sourceId - source id we want to check for updates
 * @param req.domain - Greater jurisdiction we are querying updates for, usually a country slug
 * @param req.jurisdictions - Array of jurisdictions within the req.domain that we need to query updates for
 */
app.post('/get-updates', initialNewPayloadValidator(), initialNewPayloadValidatorCustom, async (req, res) => {
  console.log(req.body);
  const captureParams: ICaptureParameters = {
    sourceId: req.body.sourceId,
    domain: req.body.domain,
    jurisdictions: req.body.jurisdictions,
    jobId: uuidv4(),
    jobType: "getUpdates"
  };

  captureService.getUpdates(captureParams);
  res.status(200).json({ error: null, message: `Capture started for: ${captureParams.domain || captureParams.jurisdictions}`, jobId: captureParams.jobId });
});

/**
 * Fetched work with given id, converts to LegalDoc HTML and stores on S3
 * @param req.work_expression_id - work expression id we want to check for updates
 * @param req.original_doc_id to be posted back when linking orig doc+expression+legalDoc ids
 * @param req.path - key on S3
 * @param req.sourceId - the source id|slug this work belongs to -- will run before_convert hook
 * Don't pass optimus script id because it is either set same as sourceId OR set in before_convert hook
 */
app.post('/convert-to-legaldoc', convertPayloadValidator(), defaultPayloadValidatorCustom, upload.single('doc'), async (req, res) => {
  const convertParams: ICaptureParameters = {
    sourceId: (req as any).headers.sourceid,
    mimeType: (req as any).file.mimetype,
    fileExtension: (req as any).file.mimetype.split("/")[1],
    doc: (req as any).file.buffer.toString(),
    jobId: uuidv4(),
    jobType: "toLegaldoc"
  };

  captureService.toLegaldoc(convertParams);
  res.status(200).json({ error: null, message: `Initiated source conversion for: ${convertParams}`, jobId: convertParams.jobId });
});


app.get('/logs', async (req, res) => {

  try {
    const logs = await store.fetch_logs();
    res.status(200).json({ error: null, logs});
  } catch (error) {
    console.error(error);
    res.status(400).json({
      error: { message: error.message, errorName: error.name || error.args, error: error },
      message: null,
    });
  }
});

app.listen(port, () => console.log(`Arachno service listening on port ${port}!`));
