
export const messages = {
  SOURCE_ID_MISSING_DEFAULT: "'sourceId' has an invalid value!",
  SOURCE_ID_REQUIRED: "'sourceId' required",
  SOURCE_ID_TOO_SHORT: "'sourceId' min length 2",

  SOURCE_UNIQUE_ID_MISSING_DEFAULT: "'source_unique_id' has an invalid value!",
  SOURCE_UNIQUE_ID_REQUIRED: "'source_unique_id' required",
  SOURCE_UNIQUE_ID_TOO_SHORT: "'source_unique_id' min length 2",

  DOMAIN_MISSING_DEFAULT: "'domain' has an invalid value!",
  DOMAIN_REQUIRED: "'domain' required and cannot be empty string!",
  DOMAIN_TOO_SHORT: "'domain' min length 2",
  
  JURISDICTIONS_MISSING_DEFAULT: "'jurisdictions' val failed validation!",
  JURISDICTIONS_ARR_REQUIRED: "'jurisdictions' required",
  JURISDICTIONS_MUST_BE_ARRAY: "'jurisdictions' should be a string array and cannot be empty",

  START_URL_MISSING_DEFAULT: "'startUrl' val failed validation!",
  START_URL_REQUIRED: "'startUrl' required",
  START_URL_TOO_SHORT: "'startUrl' min length 2",
  START_URL_INVALID_FORMAT: "'startUrl' is not a valid URL",

  CONTENT_HASH_MISSING_DEFAULT: "'content_hash' val failed validation!",
  CONTENT_HASH_REQUIRED: "'content_hash' required",
  CONTENT_HASH_TOO_SHORT: "'content_hash' required length 32",

  WORK_EXPRESSION_ID_MISSING_DEFAULT: "'work_expression_id' has an invalid value!",
  WORK_EXPRESSION_ID_REQUIRED: "'work_expression_id' required",
  WORK_EXPRESSION_ID_NOT_INT: "'work_expression_id' should be an integer > 0",

  ORIGINAL_DOC_ID_MISSING_DEFAULT: "'original_doc_id' has an invalid value!",
  ORIGINAL_DOC_ID_REQUIRED: "'original_doc_id' required",
  ORIGINAL_DOC_ID_NOT_INT: "'original_doc_id' should be an integer > 0",

  MIMETYPE_MISSING_DEFAULT: "'mimeType' has an invalid value!",
  MIMETYPE_REQUIRED: "'mimeType' required",
  MIMETYPE_TOO_SHORT: "'mimeType' min length 4",

  FILE_EXTENSION_MISSING_DEFAULT: "'fileExtension' has an invalid value!",
  FILE_EXTENSION_REQUIRED: "'fileExtension' required",
  FILE_EXTENSION_TOO_SHORT: "'fileExtension' min length 2",

  DOC_CONTENT_MISSING_DEFAULT: "'doc' has an invalid value!",
  DOC_CONTENT_REQUIRED: "'doc' required",
  DOC_CONTENT_TOO_SHORT: "'doc' min length 2",

};