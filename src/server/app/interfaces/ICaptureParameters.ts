export default interface ICaptureParameters {
  sourceId?: string; // source config slug - its unique identifier
  domain?: string; // Parent Jurisdiction - typically a country but not always
  jurisdictions?: string[]; // Array of jurisdictions within the Domain
  sourceUniqueId?: string; // Unique ID of work to be captured
  startUrl?: string;
  content_hash?: string; // Passed in when checking diffs
  fileExtension?: string;
  mimeType?: string;
  doc?: string;
  jobId: string;
  jobType: "initialCrawl" | "getMetadata" | "getUpdates" | "getDoc" | "toLegaldoc";
}
