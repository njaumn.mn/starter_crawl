export default interface IDocument {
  doc: string;
  name: string;
  mimeType: string;
  extension: string;
  hash: string;
  readonly sourceSlug: string;
  /**
   * By default matches source slug but if source implements multiple Optimus cripts the this value
   * should be conditionally set from crawler script for each document
   */
  optimusScript: string;

  generateHash(): IDocument;

  getFileSize(): Number;
}
