import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import IDocument from '@app/interfaces/IDocument';
import ICaptureParameters from './ICaptureParameters';

export default interface ICapture {
  readonly params: ICaptureParameters;
  readonly sourceSlug: string;
  readonly jurisdictions: string[];
  readonly domain: string; // Main|Parent Jurisdiction

  init(): Promise<void>;
  setParameters(params: ICaptureParameters): void;
  getMetaData(url: string): Promise<IWorkMetaData | void>;
  getDoc(url: string): Promise<IDocument | void>;
  initialCrawl(jurisdiction?: string[]): Promise<string[] | void>;
  getUpdates(jurisdiction?: string[]): Promise<string[] | void>;
  cleanup(): Promise<void>;

  beforeConvertHook(doc: IDocument): Promise<IDocument>;
  toLegalDoc(doc: IDocument): Promise<IDocument>;
  postProcess(doc: IDocument): Promise<IDocument>;
}

export type dataTypes = typeof _dataTypes[number];

const _dataTypes = [
  'article',
  'alinea',
  'book',
  'chapter',
  'division',
  'hcontainer',
  'indent',
  'level',
  'list',
  'part',
  'paragraph',
  'point',
  'proviso',
  'rule',
  'section',
  'subchapter',
  'clause',
  'subclause',
  'subdivision',
  'sublist',
  'subparagraph',
  'subpart',
  'subrule',
  'subsection',
  'subtitle',
  'title',
  'tome',
  'transitional',
];