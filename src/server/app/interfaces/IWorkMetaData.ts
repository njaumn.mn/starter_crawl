export default interface IWorkMetaData {
  title: string;
  language_code: string;
  primary_location_id: number;
  source_unique_id: string;
  source_id: number;
  start_url: string;
  effective_date: Date;
  content_hash: string;
  title_translation: string;
  work_number?: string;
  publication_number?: string;
  publication_document_number?: string;
  issuing_authority_id?: number;
  work_type?: workTypes;
  work_date?: Date;
  readonly sourceSlug: string;
}

export type workTypes = typeof _workTypes[number];

const _workTypes = [
  'act',
  'agreement',
  'amendment',
  'announcement',
  'bill',
  'bylaw',
  'circular',
  'code',
  'code_of_practice',
  'constitution',
  'decision',
  'declaration',
  'decree',
  'direction',
  'directive',
  'enrolled_bill',
  'executive_order',
  'final_rule',
  'guidance',
  'guidance_notes',
  'guidelines',
  'interim_final_rule',
  'law',
  'legal_update',
  'legislation',
  'legislative_instrument',
  'measures',
  'ministerial_order',
  'notice',
  'notification',
  'order',
  'ordinance',
  'policy',
  'procedures',
  'proclamation',
  'proposal',
  'regulation',
  'resolution',
  'rule',
  'standard',
  'statutory_instrument'
] as const;
