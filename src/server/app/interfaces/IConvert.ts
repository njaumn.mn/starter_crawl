import IDocument from '@app/interfaces/IDocument';

export default interface IConvert {
  htmlToLegalDocHtml(doc: IDocument): Promise<IDocument>;
  toHtml(doc: IDocument): Promise<IDocument>;
}
