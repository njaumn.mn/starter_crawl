import ICapture from "@app/interfaces/ICapture";
import ICaptureParameters from "@app/interfaces/ICaptureParameters";
import ConfigFactory from "@app/modules/ConfigFactory";
import Document from "@app/modules/Document";
import Store from "./Store";


class Capture
 {

    private job_store: Store;

    constructor() {
        this.job_store = new Store();
    }

    async initialCrawl(captureParams: ICaptureParameters) {
      try {
        await this.job_store.create(captureParams);
        const config = await this.loadConfig(captureParams);
        const discoveredLinks = await config.initialCrawl(captureParams.jurisdictions || []);
        const output = JSON.stringify(discoveredLinks);
        await this.job_store.update("finished", captureParams, output, null);
        
        return Promise.resolve();
      } catch (error) {
        console.log(`Job: ${captureParams.jobId} failed with error:`, error);
        await this.job_store.update("failed", captureParams, null, error.message);

        return Promise.reject(error);
      }
    }

    async getUpdates(captureParams: ICaptureParameters){
      try {
        await this.job_store.create(captureParams);
        const config = await this.loadConfig(captureParams);
        const discoveredLinks = await config.getUpdates(captureParams.jurisdictions || []);
        const output = JSON.stringify(discoveredLinks);
        await this.job_store.update("finished", captureParams, output, null);

        return Promise.resolve();
      } catch (error) {
        console.log(`Job: ${captureParams.jobId} failed with error:`, error);

        if (typeof error == "object") {
          error = JSON.stringify({
            message: error.message,
            stack: error.stack || ""
          });
        }
        await this.job_store.update("failed", captureParams, null, error);

        return Promise.reject(error);
      }
    }

    async getMetadata(captureParams: ICaptureParameters){
      try {
        await this.job_store.create(captureParams);
        const config = await this.loadConfig(captureParams);
        const metadata = await config.getMetaData(captureParams.startUrl);
        const output = JSON.stringify(metadata);
        await this.job_store.update("finished", captureParams, output, null);

        return Promise.resolve();
      } catch (error) {
        console.log(`Job: ${captureParams.jobId} failed with error:`, error);

        if (typeof error == "object") {
          error = JSON.stringify({
            message: error.message,
            stack: error.stack || ""
          });
        }
        await this.job_store.update("failed", captureParams, null, error);

        return Promise.reject(error);
      }
    }

    async getDoc(captureParams: ICaptureParameters){
      try {
        await this.job_store.create(captureParams);
        const config = await this.loadConfig(captureParams);
        const doc = await config.getDoc(captureParams.startUrl);

        if (!doc) {
          throw new Error("Failed to get Doc, nothing returned! Verify your scraper method is working correctly!");          
        }

        const output = JSON.stringify(doc);
        await this.job_store.update("finished", captureParams, output, null);

        return Promise.resolve();
      } catch (error) {
        console.log(`Job: ${captureParams.jobId} failed with error:`, error);
        
        if (typeof error == "object") {
          error = JSON.stringify({
            message: error.message,
            stack: error.stack || ""
          });
        }
        await this.job_store.update("failed", captureParams, null, error);

        return Promise.reject(error);
      }
    }

    async toLegaldoc(captureParams: ICaptureParameters){
      try {
        await this.job_store.create(captureParams);
        const config = await this.loadConfig(captureParams);
        const doc = new Document(captureParams.sourceId);
        doc.extension = captureParams.fileExtension;
        doc.mimeType = captureParams.mimeType;
        doc.doc = captureParams.doc;

        const legaldoc = await config.toLegalDoc(doc);
        if (!legaldoc) {
          throw new Error("Failed to get Doc, nothing returned! Verify your scraper method is working correctly!");          
        }

        const output = JSON.stringify(legaldoc);
        await this.job_store.update("finished", captureParams, output, null);

        return Promise.resolve();
      } catch (error) {
        console.log(`Job: ${captureParams.jobId} failed with error:`, error);

        if (typeof error == "object") {
          error = JSON.stringify({
            message: error.message,
            stack: error.stack || ""
          });
        }
        await this.job_store.update("failed", captureParams, null, error);
        return Promise.reject(error);
      }
    }

    /**
   * Loads source config using the sourceId. MUST have sourceId during single doc capture
   * which is the case for this class.
   * @param captureParams
   */
  private loadConfig(captureParams: ICaptureParameters): Promise<ICapture> {
    return new Promise(async (resolve, reject) => {
      try {
        const configFactory = new ConfigFactory();
        const captureService = configFactory.getConfig(captureParams).shift();
        await captureService.init();
        resolve(captureService);
        return;
      } catch (e) {
        return reject(e);
      }
    });
  }
}

export default Capture;