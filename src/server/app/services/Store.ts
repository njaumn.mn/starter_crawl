import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import knex from 'knex';
import {resolve} from 'path';


const options = {
    client: 'sqlite3',
    connection: {
      filename: resolve('./db/yangu')
    },
    seeds: {
      directory: resolve('./db/seeds/development')
    },
    migrations: {
      directory: resolve('./db/migrations/development')
    },
    useNullAsDefault: true
};

class Store {
    constructor() {}

    async create(input: ICaptureParameters){

        const _knex = knex(options);
        const jobData = {
            jobId: input.jobId,
            status: "started",
            type: input.jobType,
            input: JSON.stringify(input)
        };
        try {      
            await _knex('job').insert(jobData).timeout(10000);

            console.log('Store.save: Job created! Id: ', input.jobId);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(`Saving job ${input.jobId} data to DB has failed: ${error}`);
        } finally {
            _knex.destroy();
        }

    }

    async update(status: "failed" | "finished", input: ICaptureParameters, output: any, error?: string){
        const _knex = knex(options);
        const jobData = {
            status,
            output: JSON.stringify(output),
            errors: error
            };

        try {      
            await _knex('job').where('jobId', '=', input.jobId)
            .update(jobData).timeout(10000);

            console.log(`Store.update: Job Id: ${input.jobId} status updated '${status}'!`);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(`Logging job ${input.jobId} error to DB has failed: ${error}`);
        } finally {
            _knex.destroy();
        }
    }

    async fetch_logs(){
        const _knex = knex(options);

        try {      
            const logs = await _knex.raw(`SELECT * FROM (
                SELECT * FROM job ORDER BY id DESC LIMIT 5
            ) sub
            ORDER BY id ASC`).timeout(10000);

            
            return logs;
        } catch (error) {
            console.error(`Store.fetch_logs: Fetch logs failed!`, error);
            return Promise.reject(`Fetch logs failed! ${error}`);
        } finally {
            _knex.destroy();
        }
    }
}

export default Store;