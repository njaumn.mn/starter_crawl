import _ from 'lodash';

import * as sources from '@sources/index';
import CaptureBase from './CaptureBase';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import ICapture from '@app/interfaces/ICapture';

export default class ConfigFactory {
  private _sourceIdConfigsMap: any;

  private _jurisdictionsConfigsMap: any;

  constructor() {
    this.createSourceIdConfigsMap();
    this.createJurisdictionsConfigsMap();
  }

  /**
   * Attempts to autoload Config instances given either sourceId|slug OR
   * Domain OR domain and jurisdictions in domain
   * If sourceSlug set ignore the domain and jurisdiction
   * Get matching config instance
   * If domain and jurisdictions set return all configs of matching jurisdictions in that domain
   * If just domain set and jurisdictions empty, get
   * all configs of that jurisdiction - will do complete crawl of that jurisdiction
   * If domain and sourceSlug not set then halt, Error!
   * Base config shall implement util method to filter
   * passed in jurisdictions to return subset of only those current config implements
   * @param captureParams
   */
  public getConfig(captureParams: ICaptureParameters): ICapture[] {
    const sourceSlug = captureParams.sourceId;
    const domain = captureParams.domain;
    const jurisdictions = captureParams.jurisdictions;

    try {
      if ((!domain && !sourceSlug) || (domain && sourceSlug)) {
        throw new Error('Error: Both Domain and Source Slug cannot be empty|set at the same time!');
      } else if (sourceSlug) {
        return [this.getBySourceId(sourceSlug)];
      } else {
        // Domain is set
        // get array of configs of this domain that implement any of the jurisdictions passed
        // if none passed then instances for all jurisdictions in domain
        return this.getByDomainJurisdictions(domain, jurisdictions);
      }
    } catch (error) {
      throw new Error(
        `Error: Loading configs for capture params: '${JSON.stringify(
          captureParams
        )}' failed! Output error: ${error}`
      );
    }
  }

  /**
   * Returns an instance of Source Config with the matching sourceSlug
   * @param sourceSlug - Unique identifier of this source config, matches the source id
   */
  public getBySourceId(sourceSlug: string): ICapture {
    try {
      const conf = this._sourceIdConfigsMap[sourceSlug];
      return new conf();
    } catch (error) {
      throw new Error(`Config '${sourceSlug}' does not exist!`);
    }
  }

  /**
   * Given domain returns array of config INSTANCES that implement the domain
   * if both domain and jurisdictions passed then array of config instances
   * for given jurisdictions in the domain
   * @param domain - Domain to return configs for
   */
  public getByDomainJurisdictions(domain: string, jurisdictions: string[]): ICapture[] {
    try {
      const domainJurisdictionsConfigs = this._jurisdictionsConfigsMap[domain];
      if (!domainJurisdictionsConfigs) {
        throw new Error(
          `Error: passed in domain: ${domain} does not have source configs associate with it or does not exist!`
        );
      }

      let slugsArr = [];
      if (jurisdictions) {
        jurisdictions.forEach((jur) => {
          // merge all slug arrays for the jurisdictions in this domain to single array and remove duplicates
          const jurisdiction = domainJurisdictionsConfigs[jur];
          if (jurisdiction) slugsArr = _.uniq(_.concat(slugsArr, jurisdiction));
          else throw new Error(`Jurisdiction ${jur} does not exist in domain ${domain}`);
        });
      } else {
        Object.keys(domainJurisdictionsConfigs).forEach((jur) => {
          // merge all slug arrays for the jurisdictions in this domain to single array and remove duplicates
          slugsArr = _.uniq(_.concat(slugsArr, domainJurisdictionsConfigs[jur]));
        });
      }

      // for each slug, fetch config instance and push to arr
      const configs = [];
      slugsArr.forEach((sourceSlug) => {
        configs.push(this.getBySourceId(sourceSlug));
      });

      return configs;
    } catch (error) {
      throw new Error(
        `Error: Load Configs for passed in Domain: '${domain}' failed! Output error: ${error}`
      );
    }
  }

  /**
   * Used to create map that enabled loading of configs by domain or jurisdiction
   * Builds a map of
   *      { domain(s): { jurisdiction(s) in domain: [Array of slugs implementing this jurisdiction] }}
   */
  private createJurisdictionsConfigsMap() {
    this._jurisdictionsConfigsMap = {};
    for (const conf in sources) {
      if (Object.prototype.hasOwnProperty.call(sources, conf)) {
        const classObj = sources[conf];
        const obj = new classObj();
        if (obj instanceof CaptureBase && obj.domain) {
          if (!this._jurisdictionsConfigsMap[obj.domain]) {
            this._jurisdictionsConfigsMap[obj.domain] = {};
          }
          obj.jurisdictions.forEach((jurisdiction) => {
            if (!this._jurisdictionsConfigsMap[obj.domain][jurisdiction]) {
              this._jurisdictionsConfigsMap[obj.domain][jurisdiction] = [];
            }
            this._jurisdictionsConfigsMap[obj.domain][jurisdiction].push(obj.sourceSlug);
          });
        }
      }
    }
  }

  /**
   * Used to map config slugs to to their respective instances
   * to dynamically load instances
   * Builds a map of { sourceIds|slugs: source config instace}
   */
  private createSourceIdConfigsMap() {
    this._sourceIdConfigsMap = {};
    for (const conf in sources) {
      if (Object.prototype.hasOwnProperty.call(sources, conf)) {
        const classObj = sources[conf];
        const obj = new classObj();
        if (obj instanceof CaptureBase) {
          if (obj.sourceSlug in this._sourceIdConfigsMap) {
            // Key Already exists
            throw new Error(
              `Duplicate sourceSlugs for ${obj.sourceSlug} detected. Must rename one!`
            );
          }
          this._sourceIdConfigsMap[obj.sourceSlug] = classObj;
        }
      }
    }
  }
}
