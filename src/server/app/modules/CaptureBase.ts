import ICapture, { dataTypes } from '@app/interfaces/ICapture';
import ICaptureParameters from '@app/interfaces/ICaptureParameters';
import IWorkMetaData from '@app/interfaces/IWorkMetaData';
import IDocument from '@app/interfaces/IDocument';
import { Browser, BrowserContext, chromium, Page } from 'playwright';
import _ from 'lodash';
import config from '../../config';
import * as scripts from '@vendor/singlefile/back-ends/common/scripts';
import fetch from 'node-fetch';
import HTMLCleaner from './HTMLCleaner';
import cuid from 'cuid';

declare const global: ArachnoGlobal;
declare const singlefile;

const env = config.get('env');
const headless = env === 'development' ? false : true;
export default abstract class CaptureBase implements ICapture {
  /**
   * The unique identifier for source script
   */
  public abstract sourceSlug: string;

  public params: ICaptureParameters;

  public abstract domain: string;

  public abstract jurisdictions: string[];

  private htmlCleaner:  HTMLCleaner;

  protected abstract dataTypesHierarchy: Array<dataTypes | Array<dataTypes>> = [];

  /**
   * The Browser object from Playwright
   */
  static browser: Browser;
  browserCtx: BrowserContext;

  constructor(parameters) {
    this.params = parameters;
    this.htmlCleaner = new HTMLCleaner();
  }

  /**
   * Initiates Capture base class by launching a playwright browser and setting new context
   * @param options options to pass when creating a new plawright browser context
   */
  public async init(options?: any): Promise<void> {
    try {
      if (!CaptureBase.browser) {
        CaptureBase.browser = await chromium.launch({ headless, chromiumSandbox: false });
      }
      this.browserCtx = await this.getBrowserContext(options);
    } catch (e) {
      return Promise.reject(`Error launching Browser: ${e}`);
    }
  }

  public setParameters(params: ICaptureParameters) {
    this.params = params;
  }

  /**
   * Creates and returns a new playwright browser context.
   * @param options Options to pass when creating new browser context
   */
  protected async getBrowserContext(options?: object): Promise<BrowserContext> {
    try {
      let _options = {};
      if (_.isObject(options) && !_.isEmpty(options)) {
        _options = options;
      }
      const ctx = await CaptureBase.browser.newContext(_options);
      const injectedScript = await scripts.get(options);
      await ctx.addInitScript(injectedScript);

      return ctx;
    } catch (e) {
      return Promise.reject(`Error creating a new browser context: ${e}`);
    }
  }

  /**
   * Call this method to get a new playwright page
   */
  protected async getNewPage(): Promise<Page> {
    if (!this.browserCtx) {
      await this.getBrowserContext();
    }
    return await this.browserCtx.newPage();
  }

  /**
   * Saves the currently opened page as a single file, i.e will
   * replace the contents of the page with a single file version
   * Waits for the save-file extension to signal completion
   * @param page playwright.Page
   * @param options - config options to pass to singleFile
   * @param replacePageContent whether the page innerHTML should be switched with the singleFile version
   */
  public async getPageData(page: Page, options: {}, replacePageContent?: boolean): Promise<string> {
    let pageContent = await page.evaluate(
      async (options) => {
        const pageData = await singlefile.lib.getPageData(options.options);
        return pageData.content;
      },
      { options, replacePageContent }
    );

    this.beforeEmogrifyHook(pageContent);

    this.htmlCleaner.setHtmlString(pageContent);
    await this.htmlCleaner.clean();

    pageContent = this.htmlCleaner.htmlString;

    pageContent = await this.emogrify(pageContent);

    if (replacePageContent) {
      await page.evaluate(async (emogrifiedContent) => {
        document.documentElement.innerHTML = emogrifiedContent;
      }, pageContent);
    }

    return pageContent;
  }

  /**
   * Called after emogrifier and toLegalDoc runs. 
   * Clean out residual styles
   * @param pageContent page hml string content
   */
  public async postProcess(doc: IDocument){
    const pageContent = doc.doc;
    
    this.htmlCleaner.setHtmlString(pageContent);
    await this.htmlCleaner.postLegalDocClean();
    doc.doc = this.htmlCleaner.htmlString;

    await this.tagDataLevels(doc);
    
    return doc;
  }

  public async emogrify(html: string): Promise<string> {
    const host = config.get('emogrifier.host');
    let content: string;

    const response = await fetch(host, {
      method: 'post',
      body: JSON.stringify({ html }),
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });

    if (!response.ok) {
      const errorMsg = await response.text();
      console.error(`ERROR: Emogrifier convert to html failed. --- RESPONSE: `, errorMsg);
      return Promise.reject(`Request to Emogrifier Failed! ${errorMsg}`);
    }

    // If you care about a response:
    if (response.body !== null) {
      // body is ReadableStream<Uint8Array>
      // parse as needed, e.g. reading directly, or
      const body = await response.json();
      content = body['data'];
    }

    return content;
  }

  /**
   * Call this method to cleanup the browser context(s)
   * created for the current config
   */
  public async cleanup() {
    if (this.browserCtx) {
      await this.browserCtx.close();
    }
  }

  /**
   * generateId returns a new CUID slug
   */
  public generateId() {
    return cuid.slug();
  }

  /**
   * Autogenerates the data-level property for all data-types based on the live hierarchy
   * @param doc 
   */
  private async tagDataLevels(doc: IDocument): Promise<IDocument | void>{
    try {
      const dom = (new global.ARACHNO.DOMParser())
        .parseFromString(doc.doc, 'text/html');4

      if (this.dataTypesHierarchy.length > 0) {
        let hierachyTracker = {};
        const dataTypes: HTMLElement[] = dom.querySelectorAll('[data-type]');
        for (let i = 0; i < dataTypes.length; i++) {
          const currentElem = dataTypes[i];
          const currentDataType = currentElem.getAttribute('data-type');

          const dataTypesFlattened = _.flatten(this.dataTypesHierarchy);
          const currentTypeIndex = dataTypesFlattened.indexOf(currentDataType);
          const higherDataTypes = _.slice(dataTypesFlattened, 0, currentTypeIndex).reverse();

          if (currentDataType in hierachyTracker) {
            currentElem.setAttribute('data-level', hierachyTracker[currentDataType]);
            hierachyTracker = _.pick(hierachyTracker, [currentDataType, ...higherDataTypes]);
            continue;
          }
          
          for (const dataType of higherDataTypes) {
            if (dataType in hierachyTracker) {
              hierachyTracker = _.pick(hierachyTracker, [currentDataType, ...higherDataTypes]);
              const prevTypeIndex = _.findIndex(this.dataTypesHierarchy, (arr)=>{ return arr==dataType || (Array.isArray(arr) && arr.includes(dataType)) });
              const currTypeIndex = _.findIndex(this.dataTypesHierarchy, (arr)=>{ return arr==currentDataType || (Array.isArray(arr) && arr.includes(currentDataType)) });
              if (prevTypeIndex === currTypeIndex) {
                hierachyTracker[currentDataType] = hierachyTracker[dataType];
              } else {
                hierachyTracker[currentDataType] = hierachyTracker[dataType] + 1;
              }            
              break;
            }
          }
          
          if (!(currentDataType in hierachyTracker)) {
            hierachyTracker = { [currentDataType]:  1 }; // reset hierarachyTracker -- No higher dataType
          }

          currentElem.setAttribute('data-level', hierachyTracker[currentDataType]);
        }

        global.ARACHNO.logger.info({ message: "Data levels successfully generated!" });
      }
      
      doc.doc = dom.documentElement.outerHTML;
      return doc;
    } catch (e) {
      global.ARACHNO.logger.error({"message": `Error generating data-levels`, "error": e});
      
    }
    
  }

  /**
   * Given URL to a single legal work on a source, should extract and return a IWorkMetaData object
   * as per the Arachno service spec.
   * @param url
   */
  public abstract getMetaData(url: string): Promise<IWorkMetaData | void>;

  /**
   * Given a URL to a single legal work on a source should extract the legal work closest bounding container
   * such that only the legal work is returned. Watch out for copyrights and unwanted elements that may be scraped
   * in - need to preprocess before returning the legal work.
   *
   * If PDF or Word document, return it as is base64 encoded and stored in the doc.doc property
   * If html then return it having cleaned out unneccesary elements, should only be the legalwork
   * can try to retain the parent containers if they are being used to target elements on the page -legal work - when
   * applying styling. should call getPageData() method to process the page before extracting the legal work.
   * @param url
   */
  public abstract getDoc(url: string): Promise<IDocument | void>;

  /**
   * Discovery method that returns string array with all discovered urls for the
   * type of works we are interested in scraping in from the source.
   * @param jurisdictions - may sometimes be interested in specific jurisdiction in a domain, this will be posted during request
   */
  public abstract initialCrawl(jurisdictions?: string[]): Promise<Array<any> | void>;

  /**
   * Returns string array with URLs to discovered updates on the source.
   * @param jurisdiction - optional string array passed when only interested in fetching updates for specific jurisdictions on the source
   */
  public abstract getUpdates(jurisdiction?: string[]): Promise<string[] | void>;

  /**
   * Lifecycle function
   *
   * Always called before converting document
   * If Document was PDF or word, it will have already been converted to HTML
   *
   * Opportunity to set the Optimus conversion script for this document based on the
   * detected structure
   * @param doc - The document being converted
   */
  public abstract beforeConvertHook(doc: IDocument): Promise<IDocument>;

  /**
   * Lifecycle function
   *
   * Always called after singlefiling but before emogrifying document
   * Document will already have been run through single file
   *
   * Opportunity to cleanup the document before it goes through emogrifier
   * @param doc - The document being converted
   */
  public abstract beforeEmogrifyHook(doc: String): Promise<String>;

  /**
   * Lifecycle function
   *
   * Pipeline function called after beforeConvertHook
   * to enrich html document with legalDoc properties and attributes
   * Add source specific tagging code here
   * @param doc the HTML Document
   */
  public abstract toLegalDoc(doc: IDocument): Promise<IDocument>;
}
