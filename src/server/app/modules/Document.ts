import md5 from 'md5';

import IDocument from '@app/interfaces/IDocument';

export default class Document implements IDocument {
  doc: string;
  name: string;
  mimeType: string;
  extension: string;
  hash: string;
  readonly sourceSlug: string;
  optimusScript: string;

  constructor(sourceSlug: string) {
    this.sourceSlug = sourceSlug;
    this.optimusScript = sourceSlug;
  }

  /**
   * Generate and sets md5 of current
   * @param doc
   */
  public generateHash(): Document {
    this.hash = md5(this.doc);
    this.name = `${this.hash}.${this.extension}`;
    return this;
  }

  /**
   * Gets the size of the Current document in Bytes
   */
  public getFileSize(): number {
    // const buff = Buffer.from(doc.doc);
    // const buff = Buffer.from(data, 'base64');
    let buff: Buffer;
    if (!this.extension || !['pdf', 'docx', 'doc', 'html', 'htm'].includes(this.extension)) {
      throw new Error(
        'Error calculating document size. Unknown file type or document content|extension'
      );
    }
    if (['html', 'htm'].includes(this.extension)) {
      buff = Buffer.from(this.doc);
    } else if (['pdf', 'docx', 'doc'].includes(this.extension)) {
      buff = Buffer.from(this.doc, 'base64');
    }
    return buff.byteLength;
  }
}
