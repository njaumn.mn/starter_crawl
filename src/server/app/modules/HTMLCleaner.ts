
declare const global: ArachnoGlobal;
export default class HTMLCleaner {

    public htmlString: string;

    constructor(html?:string) {
      this.htmlString = html;
    }

    public setHtmlString(html: string){
      this.htmlString = html;
    }
  
    /**
     * Remove unwanted page elements
     */
    public async clean(): Promise<void> {
      this.cleanStyles();
    }

    /**
     * Removes unnecessary elements after the toLegalDoc method
     * has run
     */
    public async postLegalDocClean(){

      const unwantedStyles = String.raw`(animation|animation-delay|animation-direction|animation-duration|            //Matches all unwanted style properties
        animation-fill-mode|animation-iteration-count|animation-name|animation-play-state|animation-timing-function|
        backface-visibility|background-color|border-top-color|box-shadow|color|cursor|font|font-family|font-size|font-size-adjust|
        font-stretch|font-variant|letter-spacing|list-style-image|outline|outline-color|outline-offset|outline-style|outline-width|
        text-decoration-color|transition|transition-delay|transition-duration|transition-property|transition-timing-function|z-index|line-height):[^;"']*?;`;

      const unwantedStylesRx = new RegExp(
        unwantedStyles.replace(/(?: *\/\/(?:(?!\/\/).)*)?\n/g, ''),
        'gim'
      );

      this.htmlString = this.htmlString
        .replace(/<head.*?>[\s\S]*?(?=<\/head>)/gim, '')
        .replace(/<script.*?>[\s\S]*?(?=<\/script>)/gim, '')
        .replace(/<!--[\s\S]*?-->/gim, '')
        .replace(/data-(?!level|type|id|inline|is_section).*?=("|').*?\1/gim, '')
        .replace(/class=("|')[\s\S]*?\1/gim, '')
        .replace(unwantedStylesRx, '')
        .replace(/(?:-(?:webkit|moz)-)?box-sizing:\s*border-box;/gim, '')
        .replace(/(?<=style=("|'))\s+/gim, '') // style space after opening quote
        .replace(/(?<=style=("|')[^>]*?;)\s+\1/gim, '$1') // style space before closing quote
        .replace(/(?<=style=("|').*?)\s+(?=.*?\1)/gim, ' ') // convert multi-spaces to single in style props
        .replace(/style=("|')\s*?\1/gim, '') // remove empty styles
        .replace(/\s+>/gim, '>');

        this.formatLinks();
    }

    /**
     * Clean up unwanted CSS rules before sending to Emogrifier
     */
    private cleanStyles() {
      this.htmlString = this.htmlString
        .replace(/:(?:first|last)-of-type/gim, '');
    }

    /**
     * External links should open in new page
     */
    private formatLinks(){
      const dom = (new global.ARACHNO.DOMParser())
      .parseFromString(this.htmlString, 'text/html');

      const links = dom.querySelectorAll('a[href]');

      for (let i = 0; i < links.length; i++) {
        const link = links[i];
        if (/^(?:http|\.|\/)/.test(link.href)) {
          link.target = '_blank';
        }
      }

      this.htmlString = dom.documentElement.outerHTML;
    }

}