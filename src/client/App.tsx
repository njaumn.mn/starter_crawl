import { random } from 'lodash';
import * as React from 'react';
const Inspector = require('react-json-inspector');

class App extends React.Component<IAppProps, IAppState> {
	constructor(props: IAppProps) {
		super(props);
		this.state = {
			error: null,
			logs: []
		};
		
		setInterval(()=> this.fetchLogs(), 4000);
	}

	async componentDidMount() {
		this.fetchLogs();
	}

	async fetchLogs() {
		try {
			let r = await fetch('/logs');
			let result = await r.json();
			let logsAll = [...this.state.logs, ...result.logs];
			const logsMap:any = {};
			for (let i = 0; i < logsAll.length; i++) {
				const log:ILog = logsAll[i];
				logsMap[log.jobId] = log;
			}
			
			 this.setState(prevState => ({
				 error: result.error,
				 logs: Object.values(logsMap)
			  }))
			 
		} catch (error) {
			console.log(error);
		}
	}

	render() {
		return (
			<main className="container my-5">
				<h2 className="text-primary text-center">Arachno Logger</h2>
				<div className="form-group">
				<Inspector data={ this.state.logs } onClick={this.onPairClicked} />
				</div>
			</main>
		);
	}

	onPairClicked(data: any){
		console.log(data);
		
	}
}

export interface ILog {
	id: number;
	jobId: string;
	type: string;
	input: string;
	output: string;
	errors: string;
}


export interface IAppProps {}

export interface IAppState {
	error: string;
	logs: ILog[]
}

export default App;
